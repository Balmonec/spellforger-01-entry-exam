using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace CavesOfTheDragonAge
{
    internal static class ResourceExtractor
    {
        private static List<string> _extractedAssemblies = new List<string>();

        /// <summary>
        /// Extract all embedded resource files into the current directory.
        /// </summary>
        /// <returns>Returns a boolean value indicating if the extraction was successful or not.</returns>
        public static void ExtractAllResourceFiles()
        {
            // Get Current Assembly reference
            Assembly currentAssembly = Assembly.GetExecutingAssembly();
            extractFromAssembly(currentAssembly);
        }

        private static void extractFromAssembly(Assembly currentAssembly)
        {
            if (_extractedAssemblies.Contains(currentAssembly.FullName))
            {
                return;
            }
            // Get all embedded resources
            List<string> arrResources = new List<string>(currentAssembly.GetManifestResourceNames());
            arrResources.RemoveAll(x => x.EndsWith(".Properties.Resources.resources"));

            mRecreateResources(arrResources, currentAssembly);
            _extractedAssemblies.Add(currentAssembly.FullName);

            extractFromNewAssemblies();
        }

        private static void extractFromNewAssemblies()
        {
            var filesInDirectory = Directory.EnumerateFiles(".").Select(x => x.ToLower());
            var assemblies = filesInDirectory.Where(x => x.EndsWith(".dll") || x.EndsWith(".exe"));
            foreach (var assemblyName in assemblies)
            {
                try
                {
                    var assembly = Assembly.LoadFrom(assemblyName);
                    extractFromAssembly(assembly);
                }
                catch (BadImageFormatException exception)
                {
                    // its ok every thing is fine trust me
                }
            }
        }

        /// <summary>
        /// Extract embedded resources to disk.
        /// Originally from <a href="http://www.experts-exchange.com/Programming/Languages/.NET/Q_28337549.html">http://www.experts-exchange.com/Programming/Languages/.NET/Q_28337549.html</a>,
        /// but modified slightly to fit my needs.
        /// </summary>
        private static void mRecreateResources(List<string> arrResources, Assembly assembly)
        {
            foreach (string resourceName in arrResources)
            {
                //Name of the file saved on disk
                string saveAsName = resourceName;
                string currentNamespace;
                try
                {
                    currentNamespace = assembly.EntryPoint.DeclaringType.Namespace;
                }
                catch
                {
                    currentNamespace = assembly.GetName().Name;
                }
                //currentNamespace = currentNamespace.Substring(currentNamespace.IndexOf('.'));
                if (resourceName.StartsWith(currentNamespace))//Length > assembly.GetName().Name.Length+1)
                {
                    //TODO Figure out a better way to remove the namespace from the resource.
                    saveAsName = resourceName.Substring(currentNamespace.Length+1);
                }
                FileInfo fileInfoOutputFile = new FileInfo(@".\" + saveAsName);
                //CHECK IF FILE EXISTS AND DO SOMETHING DEPENDING ON YOUR NEEDS
                if (!fileInfoOutputFile.Exists)
                {
                    //OPEN NEWLY CREATING FILE FOR WRITTING
                    FileStream streamToOutputFile = fileInfoOutputFile.OpenWrite();

                    //GET THE STREAM TO THE RESOURCES
                    Stream streamToResourceFile =
                        assembly.GetManifestResourceStream(resourceName);

                    //---------------------------------
                    //SAVE TO DISK OPERATION
                    //---------------------------------
                    const int size = 4096;
                    byte[] bytes = new byte[4096];
                    int numBytes;
                    while ((numBytes = streamToResourceFile.Read(bytes, 0, size)) > 0)
                    {
                        streamToOutputFile.Write(bytes, 0, numBytes);
                    }
                    //txtResults.Text += saveAsName + " Created" + Environment.NewLine;
                    streamToOutputFile.Close();
                    streamToResourceFile.Close();
                }
            }//end_foreach
        }
    }
}