﻿using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.UI.UserInterface
{
    public static class UserInterfaceFactory
    {
        //return an implementation of the IUserInterface interface
        //depending on the string passed in, for ease of configuration.
        public static IUserInterface GetUserInterface(string type = "default")
        {
            if (type == "default")
            {
                return makeDefaultUserInterface();
            }
            else
            {
                return makeDefaultUserInterface();
            }
        }

        //make a defaultUserInterface
        private static DefaultUserInterface makeDefaultUserInterface()
        {
            DefaultUserInterface userInterface = new DefaultUserInterface();
            userInterface.InitUi(80, 50, "Spellforger 1: Entry Exam");

            return userInterface;
        }
    }
}