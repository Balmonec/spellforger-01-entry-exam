﻿using System;
using System.Collections.Generic;
using System.Drawing;
using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine;
using libtcod;

namespace CavesOfTheDragonAge.UI.UserInterface
{
    partial class DefaultUserInterface : IUserInterface
    {
        public DefaultUserInterface()
        {
            PlayerId = "";
        }

        #region [-- Public Methods --]

        /// <summary>
        /// Get a CreatureAction to indicate the player's next action.
        /// </summary>
        /// <returns>A CreatureAction to indicating the player's next action.</returns>
        public CreatureAction GetPlayerAction()
        {
            CreatureAction action = new CreatureAction();

            TCODKey key = waitForKeypress(false);

            Direction direction = interpretKeyDirection(key);

            if (direction.IsHorizontalMovement())
            {
                action = move(direction);
            }
            else if (direction == Direction.Here)
            {
                action.ActionType = CreatureActionType.Wait;
            }
            else
            {
                switch (key.KeyCode)
                {
                    case TCODKeyCode.Escape:
                        int choice = DisplayMenu("Are you sure you wish to quit?", new List<string>() { "Yes", "No" }, false);
                        if (choice == 0)
                        {
                            action.ActionType = CreatureActionType.Wait;
                            GameData.EndGame = true;
                        }
                        else
                        {
                            RenderAll();
                            action = GetPlayerAction();
                        }
                        break;

                    case TCODKeyCode.KeypadDecimal:
                    case TCODKeyCode.Delete:
                        action.ActionType = CreatureActionType.Wait;
                        break;

                    case TCODKeyCode.Char:
                        switch (key.Character)
                        {
                            case 'w':
                                action = walkOrWait();
                                break;

                            case 'o':
                                action = open();
                                break;

                            case 'c':
                                action = close();
                                break;

                            case 'H':
                                action = handle();
                                break;

                            case 'z':
                                action = castMagicSpell();
                                break;

                            case 'f':
                                action = forgeMagicSpell();
                                break;

                            case '\0':
                                action = GetPlayerAction();
                                break;

                            case 'q':
                                enterDebugCommand();
                                RenderAll();
                                action = GetPlayerAction();
                                break;

                            case '.':
                                action.ActionType = CreatureActionType.Wait;
                                break;

                            case '?':
                                showHelpScreen();
                                action = GetPlayerAction();
                                break;

                            default:
                                break;
                        }
                        break;

                    default:
                        RenderAll();
                        action = GetPlayerAction();
                        break;
                }
            }
            return action;
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Returns a CreatureAction set up to close a fixture in the indicated Direction.
        /// </summary>
        /// <returns>A CreatureAction set up to close a fixture in the indicated Direction.</returns>
        private CreatureAction close()
        {
            Location playerLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(PlayerId);
            List<Direction> closeableDirections = GameData.DungeonMaps[GameData.CurrentDungeon].GetTriggerDirections(playerLocation, "CLOSE");

            if (closeableDirections.Count == 0)
            {
                //There is nothing there to close, so get another action.
                DisplayMessage("There is nothing here to close!", Color.Red);
                RenderAll();
                return GetPlayerAction();
            }
            else if (closeableDirections.Count == 1)
            {
                //There is only one thing to close, so close it.
                return new CreatureAction(CreatureActionType.Close, closeableDirections[0]);
            }
            else
            {
                //There are multiple things to close, so have the user pick one.
                Direction direction = getDirectionFromUser();

                if (direction.IsHorizontalMovement() || direction == Direction.Here)
                {
                    return new CreatureAction(CreatureActionType.Close, direction);
                }
                else
                {
                    return new CreatureAction();
                }
            }
        }

        /// <summary>
        /// Returns a CreatureAction set up to handle a fixture in the indicated Direction.
        /// </summary>
        /// <returns>A CreatureAction set up to handle a fixture in the indicated Direction.</returns>
        private CreatureAction handle()
        {
            Location playerLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(PlayerId);
            List<Direction> handleableDirections = GameData.DungeonMaps[GameData.CurrentDungeon].GetTriggerDirections(playerLocation, "HANDLE");

            if (handleableDirections.Count == 0)
            {
                DisplayMessage("There is nothing here to handle!", Color.Red);
                RenderAll();
                return GetPlayerAction();
            }
            else if (handleableDirections.Count == 1)
            {
                //There is only one thing to handle, so handle it.
                return new CreatureAction(CreatureActionType.Handle, handleableDirections[0]);
            }
            else
            {
                //There are multiple things to handle, so have the user pick one.
                Direction direction = getDirectionFromUser();

                if (direction.IsHorizontalMovement() || direction == Direction.Here)
                {
                    return new CreatureAction(CreatureActionType.Handle, direction);
                }
                else
                {
                    return new CreatureAction();
                }
            }
        }

        /// <summary>
        /// Returns a CreatureAction set up to move in the indicated Direction.
        /// </summary>
        /// <param name="direction">The Direction to move.</param>
        /// <returns>A CreatureAction set up to move in the indicated Direction.</returns>
        private CreatureAction move(Direction direction)
        {
            CreatureAction action = new CreatureAction();

            //Location location = GameData.DungeonMaps[GameData.CurrentDungeon].getCreatureLocation(this.PlayerID);
            //if (location.Item1 < 0)
            //{
            //    //TODO: handle the game not finding the player in the dungeon.
            //    throw new ApplicationException("The set player is not valid.");
            //}
            //int x = GameData.DungeonMaps[GameData.CurrentDungeon].getCreatureX(this.PlayerID)
            //    + direction.GetDeltaX();
            //int y = GameData.DungeonMaps[GameData.CurrentDungeon].getCreatureY(this.PlayerID)
            //    + direction.GetDeltaY();

            //TODO: Move this logic into the Player AI
            Location playerLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(PlayerId);

            if (GameData.DungeonMaps[GameData.CurrentDungeon].IsDirectionPassable(playerLocation, direction, PlayerId) == false)
            {
                if (GameData.DungeonMaps[GameData.CurrentDungeon].HasDirectionTrigger(playerLocation, direction, "OPEN") == true)
                {
                    //A door or something is blocking the way, so open it instead of moving.
                    //This should interrupt walking.
                    action.ActionType = CreatureActionType.Open;
                    action.ActionDirection = direction;
                }
                else
                {
                    DisplayMessage(GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId].Name + " cannot move that way.", Color.Red);
                    RenderAll();
                    action = GetPlayerAction();
                }
            }
            else if(GameData.DungeonMaps[GameData.CurrentDungeon].GetCreature(playerLocation.GetVector(direction)) != null)
            {
                //Attack the creature in the given direction.
                action.ActionType = CreatureActionType.Attack;
                action.ActionDirection = direction;
            }
            else
            {
                //All clear, move in the given direction.
                action.ActionType = CreatureActionType.Move;
                action.ActionDirection = direction;
            }

            return action;
        }

        /// <summary>
        /// Returns a CreatureAction set up to open a fixture in the indicated Direction.
        /// </summary>
        /// <returns>A CreatureAction set up to open a fixture in the indicated Direction.</returns>
        private CreatureAction open()
        {
            Location playerLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(PlayerId);
            List<Direction> openableDirections = GameData.DungeonMaps[GameData.CurrentDungeon].GetTriggerDirections(playerLocation, "OPEN");

            if (openableDirections.Count == 0)
            {
                DisplayMessage("There is nothing here to open!", Color.Red);
                RenderAll();
                return GetPlayerAction();
            }
            else if (openableDirections.Count == 1)
            {
                //There is only one thing to open, so open it.
                return new CreatureAction(CreatureActionType.Open, openableDirections[0]);
            }
            else
            {
                //There are multiple things to open, so have the user pick one.
                Direction direction = getDirectionFromUser();

                if (direction.IsHorizontalMovement() || direction == Direction.Here)
                {
                    return new CreatureAction(CreatureActionType.Open, direction);
                }
                else
                {
                    return new CreatureAction();
                }
            }
        }

        /// <summary>
        /// Returns a CreatureAction set up to walk in a specific direction, or to wait in one place.
        /// </summary>
        /// <returns>A CreatureAction set up to walk in a specific direction, or to wait in one place.</returns>
        private CreatureAction walkOrWait()
        {
            Direction direction = getDirectionFromUser();

            if (direction.IsHorizontalMovement())
            {
                return new CreatureAction() { ActionType = CreatureActionType.Walk, ActionDirection = direction };
            }
            else if (direction == Direction.Here)
            {
                return new CreatureAction() { ActionType = CreatureActionType.Waiting };
            }
            else
            {
                return new CreatureAction();
            }
        }

        /// <summary>
        /// Show the help screen.
        /// </summary>
        private void showHelpScreen()
        {
            int choice = DisplayMenu("Display Help On:", new List<string>() 
                                     { "Commands",
                                       "Spells and Runes",
                                       "Scoring",
                                       "Cancel"
                                     });

            RenderAll();

            switch (choice)
            {
                case 0:
                    DisplayMenu(@"Movement is by any of the following:" + Environment.NewLine
                              + @"Numpad:   VI:         Arrow Keys:" + Environment.NewLine
                              + @" 7 8 9     y k u       Home  UpArrow  PgUp" + Environment.NewLine
                              + @"  \|/       \|/                \|/" + Environment.NewLine
                              + @" 4-@-6     h-@-l      LeftArrow-@-RightArrow" + Environment.NewLine
                              + @"  /|\       /|\                /|\" + Environment.NewLine
                              + @" 1 2 3     b j n       End  DownArrow PgDn" + Environment.NewLine
                              + @"" + Environment.NewLine
                              + @"Numpad - 5:wait   .: wait  Delete: wait" + Environment.NewLine
                              + @"w: Enter walk mode" + Environment.NewLine
                              + @"o: open a door    c: close a door" + Environment.NewLine
                              + @"z: cast a spell   f: forge spells" + Environment.NewLine
                              + @"Tab: target closest enemy (once rune is known)" + Environment.NewLine
                              + @"Alt + Enter: toggle fullscreen" + Environment.NewLine
                              + @"Escape: opens quit prompt, and closes most menus" + Environment.NewLine
                              + @"?: Open the help menu", null);
                    RenderAll();
                    showHelpScreen();
                    break;

                case 1:
                    DisplayMenu(@"Each spell is made up of three runes, namely:" + Environment.NewLine
                              + @"  - the Target rune determines how and where the spell is "
                              + @"targeted.  For example, the caster's square, or any square "
                              + @"visible to the caster."+ Environment.NewLine
                              + @"  - the Area rune determines the area affected by the spell, "
                              + @"based on the caster's location and the target selected.  "
                              + @"For example, an area around the selected square, or a line "
                              + @"between the selected square and the caster's location." + Environment.NewLine
                              + @"  - the Effect run determines what happens to each of the squares "
                              + @"in the area.  For example, it could deal damage to creatures "
                              + @"in those squares, or open any doors in those squares." + Environment.NewLine
                              + @"" + Environment.NewLine
                              + @"You will learn runes automatically as you learn different "
                              + @"spells.  Each time you learn a new spell, you compare every "
                              + @"spell you know against each of the other spells that you know, "
                              + @"using the following three rules to determine if you learn any "
                              + @"of the runes in the spell:" + Environment.NewLine
                              + @"  - If you know all the runes in a particular spell except for one, you can automatically learn the last rune." + Environment.NewLine
                              + @"  - If an unknown rune is the only rune in common between two spells, you can learn it." + Environment.NewLine
                              + @"  - If an unknown rune is the only rune not in common between two spells, you can learn it.", null, false, 50);
                    RenderAll();
                    showHelpScreen();
                    break;

                case 2:
                    DisplayMenu(@"Points are earned for each of the following:" + Environment.NewLine
                              + @"  - Dealing damage to a monster with a spell." + Environment.NewLine
                              + @"  - Healing damage to yourself with a spell." + Environment.NewLine
                              + @"  - Learning a new spell." + Environment.NewLine
                              + @"  - Learning a new rune." + Environment.NewLine
                              + @"  - Forging a new spell." + Environment.NewLine
                              + @"  - Editing an existing spell." + Environment.NewLine
                              + Environment.NewLine
                              + @"Points are deducted for each of the following:" + Environment.NewLine
                              + @"  - Dealing damage to yourself with a spell." + Environment.NewLine
                              + @"  - Healing damage to a monster with a spell." + Environment.NewLine
                              + @"  - Dealing damage to a monster in close combat." + Environment.NewLine
                              + @"  - Taking damage from a monster in close combat.", null, false, 50);
                    RenderAll();
                    showHelpScreen();
                    break;

                default:
                    break;
            }
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        //TODO figure out a way to make this not a global variable.
        /// <summary>
        /// The ID of the creature symbolizing the Player of the game.
        /// </summary>
        public string PlayerId { get; set; }

        /// <summary>
        /// True if the user has a command waiting, false if the user does not.
        /// </summary>
        public bool UserCommandWaiting
        {
            get { return TCODConsole.checkForKeypress(1).KeyCode != TCODKeyCode.NoKey; }
        }

        #endregion [-- Properties --]
    }
}