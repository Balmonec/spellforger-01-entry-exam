﻿using System;
using System.Collections.Generic;
using System.Drawing;
using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine;
using CavesOfTheDragonAge.Engine.Creatures;
using libtcod;

namespace CavesOfTheDragonAge.UI.UserInterface
{
    internal static class ConvertColors
    {
        public static TCODColor ToTCOD(this Color color)
        {
            return new TCODColor(color.R, color.G, color.B);
        }
    }

    partial class DefaultUserInterface : IUserInterface
    {
        //TODO: Handle infinite loops of messages, and allow the user to click X to exit.

        #region [-- Public Methods --]

        /// <summary>
        /// Displays a message in the default color of white.
        /// </summary>
        /// <param name="message">The message to display.</param>
        public void DisplayMessage(string message)
        {
            DisplayMessage(message, Color.White);
        }

        /// <summary>
        /// Displays a message in the given color.
        /// </summary>
        /// <param name="message">The message to display.</param>
        /// <param name="color">The color to the message in.</param>
        public void DisplayMessage(string message, Color color)
        {
            if (message.Length <= MessagePanel.getWidth())
            {
                //message is short enough to fit on the screen, add it to the
                //message buffer and message history.
                MessageList.Add(new Tuple<string, Color>(message, color));
                MessageHistory.Add(new Tuple<string, Color>(message, color));
            }
            else
            {
                //figure out a good place to break the message - somewhere there
                //is a space.
                int position = MessagePanel.getWidth() - 1;
                char currentCharacter = message[position];
                while (currentCharacter != ' ' && position > 0)
                {
                    currentCharacter = message[position];
                    position--;
                }
                if (position > 0)
                {
                    //a good breakpoint has been found, so break the message
                    //up and display both parts.
                    DisplayMessage(message.Substring(0, position + 1), color);
                    DisplayMessage(message.Substring(position + 1).TrimStart(), color);
                }
                else
                {
                    //no good breakpoint has been found, there are no spaces
                    //in the message before the width limit, so break it there.
                    DisplayMessage(message.Substring(0, MessagePanel.getWidth()), color);
                    DisplayMessage(message.Substring(MessagePanel.getWidth()), color);
                }
            }

            //if too many messages, display a set now
            if (MessageList.Count > MessagePanel.getHeight())
            {
                RenderAll();
                TCODConsole.waitForKeypress(true);
                for (int x = 0; x < MessagePanel.getHeight(); x++)
                {
                    MessageList.RemoveAt(0);
                }
            }
        }

        /// <summary>
        /// Initialize the user interface.
        /// </summary>
        /// <param name="width">The width of the user interface.</param>
        /// <param name="height">The height of the user interface.</param>
        /// <param name="title">The title of the user interface window.</param>
        public void InitUi(int width = 100, int height = 50, string title = "Caves of the Dragon Age")
        {
            TCODConsole.setCustomFont("unicode_font.png", (int)TCODFontFlags.LayoutAsciiInRow, 32, 303);

            TCODConsole.initRoot(width, height, title, false, TCODRendererType.SDL);

            TCODConsole.mapAsciiCodesToFont(0, 65536, 0, 0);

            Con = new TCODConsole(width, height);
            MessagePanel = new TCODConsole(width, 5);
            InfoPanel = new TCODConsole(width, 5);
            DungeonPanel = new TCODConsole(width, (height - MessagePanel.getHeight() - InfoPanel.getHeight()));

            MessageList = new List<Tuple<string, Color>>();
            MessageHistory = new List<Tuple<string, Color>>();
        }

        /// <summary>
        /// render the entire screen.
        /// </summary>
        public void RenderAll(bool keepMessages = false)
        {
            //Clear all the panels
            MessagePanel.clear();
            DungeonPanel.clear();
            InfoPanel.clear();

            //display the current dungeon state.
            showDungeon();

            //load the messages onto the messagePanel
            bool allMessagesDisplayed = loadMessages();

            //show the info panel
            showInfo();

            //blit subconsoles onto the main console
            TCODConsole.blit(MessagePanel, 0, 0, MessagePanel.getWidth(), MessagePanel.getHeight(), Con, 0, 0);
            TCODConsole.blit(DungeonPanel, 0, 0, DungeonPanel.getWidth(), DungeonPanel.getHeight(), Con, 0, MessagePanel.getHeight());
            TCODConsole.blit(InfoPanel, 0, 0, InfoPanel.getWidth(), InfoPanel.getHeight(), Con, 0, MessagePanel.getHeight() + DungeonPanel.getHeight());

            // display on the main screen
            TCODConsole.blit(Con, 0, 0, Con.getWidth(), Con.getHeight(), TCODConsole.root, 0, 0);
            TCODConsole.flush();

            //if all messages not displayed, wait for the user to press a key
            if (allMessagesDisplayed == true && keepMessages == false)
            {
                MessageList.Clear();
            }
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Display the messages in the message panel.
        /// </summary>
        /// <returns>Returns true if all messages were displayed, and false if some remain to be displayed.</returns>
        private bool loadMessages()
        {
            MessagePanel.clear();

            int y = 0;
            foreach (Tuple<string, Color> mess in MessageList)
            {
                MessagePanel.setForegroundColor(mess.Item2.ToTCOD());
                MessagePanel.print(0, y, mess.Item1);
                y++;
                if (y > MessagePanel.getHeight())
                {
                    //not all messages displayed
                    TCODConsole morePanel = new TCODConsole(6, 1);
                    morePanel.setForegroundColor(TCODColor.yellow);
                    morePanel.print(0, 0, "(more)");
                    TCODConsole.blit(morePanel, 0, 0, morePanel.getWidth(),
                        morePanel.getHeight(), DungeonPanel,
                        ((DungeonPanel.getWidth() / 2) - (morePanel.getWidth() / 2)),
                        0, 1.0F, 0.7F);
                    return false;
                }
            }

            //all messages displayed
            return true;
        }

        /// <summary>
        /// Display the dungeon in the dungeon panel.
        /// </summary>
        private void showDungeon()
        {
            DungeonPanel.clear();

            Engine.Dungeons.Dungeon dungeon = GameData.DungeonMaps[GameData.CurrentDungeon];

            Location playerLocation = dungeon.GetCreatureLocation("PLAYER");

            #region Set Offsets

            int xOffset = playerLocation.X - DungeonPanel.getWidth() / 2;
            int yOffset = playerLocation.Y - DungeonPanel.getHeight() / 2;

            if (xOffset < 0)
            {
                xOffset = 0;
            }

            if (xOffset + DungeonPanel.getWidth() >= dungeon.Width)
            {
                xOffset = dungeon.Width - DungeonPanel.getWidth();
            }

            if(DungeonPanel.getWidth() >= dungeon.Width)
            {
                xOffset = dungeon.Width / 2 - DungeonPanel.getWidth() / 2;
            }

            if (yOffset < 0)
            {
                yOffset = 0;
            }

            if (yOffset + DungeonPanel.getHeight() >= dungeon.Height)
            {
                yOffset = dungeon.Height - DungeonPanel.getHeight();
            }

            if (DungeonPanel.getHeight() >= dungeon.Height)
            {
                yOffset = dungeon.Height / 2 - DungeonPanel.getHeight() / 2;
            }

            #endregion Set Offsets

            #region Calculate FOV

            int fovRadius = 10;
            TCODMap fovMap = new TCODMap(dungeon.Width, dungeon.Height);
            dungeon.RebuildCellStates();
            for(int x = 0; x < dungeon.Width; x++)
            {
                for(int y = 0; y<dungeon.Height; y++)
                {
                    fovMap.setProperties(x, y, dungeon.TransparentCells[x, y], dungeon.ImpassableCells[x, y] == false);
                }
            }
            if (dungeon.IsInDungeon(playerLocation))
            {
                fovMap.computeFov(playerLocation.X, playerLocation.Y, fovRadius, true, TCODFOVTypes.ShadowFov);
            }
            for (int x = 0; x < dungeon.Width; x++)
            {
                for (int y = 0; y < dungeon.Height; y++)
                {
                    if (dungeon.FOVState[x, y] == Engine.Dungeons.Dungeon.FOVStateType.Visible)
                    {
                        dungeon.FOVState[x, y] = Engine.Dungeons.Dungeon.FOVStateType.Explored;
                    }

                    if(fovMap.isInFov(x, y))
                    {
                        dungeon.FOVState[x, y] = Engine.Dungeons.Dungeon.FOVStateType.Visible;
                    }
                }
            }

            #endregion Calculate FOV

            for (int y = 0; y < DungeonPanel.getHeight(); y++)
            {
                for (int x = 0; x < DungeonPanel.getWidth(); x++)
                {
                    DisplayCharacter display = dungeon.GetDisplayValue(xOffset + x, yOffset + y);
                    DungeonPanel.putCharEx(x, y, display.Character, ConvertColors.ToTCOD(display.ForegroundColor), ConvertColors.ToTCOD(display.BackgroundColor));
                }
            }
        }

        /// <summary>
        /// Show the information in the info panel.
        /// </summary>
        private void showInfo()
        {
            Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[PlayerId];
            
            InfoPanel.setForegroundColor(ConvertColors.ToTCOD(GameColors.Red));
            InfoPanel.print(1, 0, "Health: " + player.Health.Current + "/" + player.Health.Maximum);

            InfoPanel.setForegroundColor(ConvertColors.ToTCOD(Color.White));
            InfoPanel.print(1, 1, "Depth : " + GameData.Depth + "/" + GameData.MaximumDepth);

            InfoPanel.setForegroundColor(ConvertColors.ToTCOD(GameColors.BlueViolet));
            InfoPanel.print(1, 2, "Target Runes Known: " + player.TargetRunesKnown.Count);

            InfoPanel.setForegroundColor(ConvertColors.ToTCOD(GameColors.BlueViolet));
            InfoPanel.print(1, 3, "Area Runes Known  : " + player.AreaRunesKnown.Count);

            InfoPanel.setForegroundColor(ConvertColors.ToTCOD(GameColors.BlueViolet));
            InfoPanel.print(1, 4, "Effect Runes Known: " + player.EffectRunesKnown.Count);

            InfoPanel.setForegroundColor(player.Score >= 0 ? ConvertColors.ToTCOD(Color.Green) : ConvertColors.ToTCOD(GameColors.Red));
            InfoPanel.print(20, 0, "Score: " + player.Score);
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        public TCODConsole Con { get; set; }

        public TCODConsole DungeonPanel { get; set; }

        public TCODConsole InfoPanel { get; set; }

        public List<Tuple<string, Color>> MessageHistory { get; set; }

        public List<Tuple<string, Color>> MessageList { get; set; }

        public TCODConsole MessagePanel { get; set; }

        #endregion [-- Properties --]
    }
}