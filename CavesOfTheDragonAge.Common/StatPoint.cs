﻿using System;

namespace CavesOfTheDragonAge.Common
{
    /// <summary>
    /// Utility class for statistics that can vary between a minimum and a maximum.
    /// </summary>
    public class StatPoint
    {
        #region Constructors

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="value">The value to set as the current and maximum allowable values.</param>
        public StatPoint(int value = 0)
        {
            Minimum = value < 0 ? value : 0;
            Maximum = value;
            Current = value;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="value">The value to set as the current.</param>
        /// <param name="max">The value to set as the maximum allowable value.</param>
        public StatPoint(int value, int max)
        {
            Minimum = value < 0 ? value : 0;
            Maximum = max;
            Current = value;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="value">The value to set as the current.</param>
        /// <param name="min">The value to set as the minimum allowable value.</param>
        /// <param name="max">The value to set as the maximum allowable value.</param>
        public StatPoint(int value, int min, int max)
        {
            Minimum = min;
            Maximum = max;
            Current = value;
        }

        #endregion Contructors

        #region Properties

        /// <summary>
        /// The current value of the statistic.
        /// </summary>
        public Int32 Current
        {
            get { return _current; }

            set
            {
                _current = Utility.Clamp(value, Minimum, Maximum);
            }
        }

        /// <summary>
        /// The minimum value of the statistic.
        /// </summary>
        public Int32 Minimum
        {
            get { return _min; }
            set
            {
                _min = value;
            }
        }

        /// <summary>
        /// The maximum value of the statistic.
        /// </summary>
        public Int32 Maximum
        {
            get { return _max; }
            set
            {
                _max = value;
            }
        }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Override the ToString function for the StatPoint class.
        /// </summary>
        /// <returns>A string in the format "Current in [Minimum, Maximum]"</returns>
        public override string ToString()
        {
            return Current + " in [" + Minimum + "," + Maximum + "]";
        }

        #endregion Public Methods

        #region Private Methods
        #endregion Private Methods

        #region Operators

        public static implicit operator int(StatPoint value)
        {
            return value.Current;
        }

        public static StatPoint operator +(StatPoint left, int right)
        {
            left.Current += right;
            return left;
        }

        public static StatPoint operator -(StatPoint left, int right)
        {
            left.Current -= right;
            return left;
        }

        /* Commenting out as unneeded, only use is for unit tests.
        /// <summary>
        /// Overload the equals operator for the StatPoint type.
        /// </summary>
        /// <param name="left">The StatPoint on the left side of the operator.</param>
        /// <param name="right">The StatPoint on the right side of the operator.</param>
        /// <returns>The Boolean that is the result of the operator.</returns>
        public static bool operator ==(StatPoint left, StatPoint right)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            return left.Current == right.Current && left.Minimum == right.Minimum && left.Maximum == right.Maximum;
        }

        /// <summary>
        /// Overload the not-equals operator for the StatPoint type.
        /// </summary>
        /// <param name="left">The StatPoint on the left side of the operator.</param>
        /// <param name="right">The StatPoint on the right side of the operator.</param>
        /// <returns>The Boolean that is the result of the operator.</returns>
        public static bool operator !=(StatPoint left, StatPoint right)
        {
            return !(left == right);
        }
        */

        #endregion Operators

        private Int32 _current;
        private Int32 _min;
        private Int32 _max;
    }
}
