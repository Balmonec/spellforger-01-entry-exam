﻿using System;

namespace CavesOfTheDragonAge.Common
{
    public static class Utility
    {
        /// <summary>
        /// Clamp the value to within the Min and Max, inclusive.
        /// </summary>
        /// <typeparam name="T">The Type that will be compared.</typeparam>
        /// <param name="value">The value to clamp.</param>
        /// <param name="min">The lower bound to clamp to, inclusive.</param>
        /// <param name="max">The upper bound to clamp to, inclusive.</param>
        /// <returns>Returns the clamped value.</returns>
        public static T Clamp<T>(T value, T min, T max) where T : IComparable<T>
        {
            if(min.CompareTo(max) > 0)
            {
                return Clamp(value, max, min);
            }

            if (value.CompareTo(min) <= 0)
            {
                return min;
            }
            if (value.CompareTo(max) >= 0)
            {
                return max;
            }

            return value;
        }
    }
}
