﻿using System;

namespace CavesOfTheDragonAge.Common
{
    public static class Randomization
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Returns a random integer between the min and max values given.
        /// </summary>
        /// <param name="min">The minimum value to return.</param>
        /// <param name="max">The maximum value to return.</param>
        /// <param name="inclusive">If false, will never return min or max, but only numbers between them.</param>
        /// <returns>A random integer between min and max.</returns>
        public static int RandomInt(int min, int max, bool inclusive = true)
        {
            int minimum = inclusive ? min : min + 1;
            int maximum = inclusive ? max + 1 : max;
            return _random.Next(minimum, maximum);
        }

        /// <summary>
        /// Returns a random boolean.
        /// </summary>
        /// <returns>A random boolean.</returns>
        public static bool RandomBool()
        {
            return RandomInt(0, 1, true) == 1 ? true : false;
        }

        #endregion [-- Public Methods --]

        #region [-- Properties --]

        private static readonly Random _random = new Random();

        #endregion [-- Properties --]
    }
}
