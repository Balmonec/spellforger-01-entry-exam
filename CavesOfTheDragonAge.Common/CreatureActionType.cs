﻿namespace CavesOfTheDragonAge.Common
{
    /// <summary>
    /// Defines the various types of actions a creature can do.
    /// </summary>
    public enum CreatureActionType
    {
        /// <summary>
        /// The CreatureActionType is undefined.
        /// </summary>
        Undefined = 0,

        /// <summary>
        /// The creature will wait, performing no action.
        /// </summary>
        Wait,

        /// <summary>
        /// The creature will move one square in a given direction.
        /// </summary>
        Move,

        /// <summary>
        /// The creature will walk in a given direction until it encounters an interruption or obstacle.
        /// </summary>
        Walk,

        /// <summary>
        /// The creature will wait in place for a number of turns.
        /// </summary>
        Waiting,

        /// <summary>
        /// The creature will open something in a given direction.
        /// </summary>
        Open,

        /// <summary>
        /// The creature will close something in a given direction.
        /// </summary>
        Close,

        /// <summary>
        /// The creature will handle something in a given direction.
        /// </summary>
        Handle,

        /// <summary>
        /// The creature will attack something in a given direction.
        /// </summary>
        Attack,

        /// <summary>
        /// The creature will cast the given spell (identified by the magnitude) at the given location.
        /// </summary>
        Cast
    }
}