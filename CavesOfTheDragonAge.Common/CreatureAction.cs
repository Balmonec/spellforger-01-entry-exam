﻿namespace CavesOfTheDragonAge.Common
{
    /// <summary>
    /// Holds the information defining a single action a creature wishes to take.
    /// </summary>
    public class CreatureAction
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Creates a new CreatureAction
        /// </summary>
        public CreatureAction(CreatureActionType actionType = CreatureActionType.Undefined, Direction actionDirection = Direction.Undefined)
        {
            ActionType = actionType;
            ActionDirection = actionDirection;
        }

        /// <summary>
        /// Validates that the required information is set for the selected ActionType.
        /// </summary>
        /// <returns>
        /// Whether the required information is set for the selected ActionType.
        /// </returns>
        public bool Validate()
        {
            switch (ActionType)
            {
                case CreatureActionType.Undefined:
                    return false;

                case CreatureActionType.Wait:
                case CreatureActionType.Waiting:
                    return true;

                case CreatureActionType.Move:
                    if (ActionDirection != Direction.Undefined)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case CreatureActionType.Walk:
                case CreatureActionType.Attack:
                    if (ActionDirection.IsHorizontalMovement())
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case CreatureActionType.Open:
                case CreatureActionType.Close:
                case CreatureActionType.Handle:
                    if (ActionDirection.IsHorizontalMovement() || ActionDirection == Direction.Here)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case CreatureActionType.Cast:
                    if(ActionLocation != null && ActionMagnitude >= 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                default:
                    return false;
            }
        }

        #endregion [-- Public Methods --]

        #region [-- Properties --]

        /// <summary>
        /// Gets or sets the direction or the action (if applicable).
        /// </summary>
        public Direction ActionDirection { get; set; }

        /// <summary>
        /// Gets or sets the type of action the creature will do.
        /// </summary>
        public CreatureActionType ActionType { get; set; }

        /// <summary>
        /// Gets or sets the magnitude of the action (if applicable).
        /// </summary>
        public int ActionMagnitude { get; set; }

        /// <summary>
        /// Gets of sets the location of the action (if applicable).
        /// </summary>
        public Location ActionLocation { get; set; }

        #endregion [-- Properties --]
    }
}