﻿using System.Drawing;

namespace CavesOfTheDragonAge.Common
{
    /// <summary>
    /// A listing of additional TCODColors.
    /// </summary>
    public static class GameColors
    {
        /// <summary>
        /// The color Azure - RGB(0, 127, 255)
        /// </summary>
        public static readonly Color Azure = Color.FromArgb(0, 127, 255);

        /// <summary>
        /// The color Blue - RGB(0, 0, 255)
        /// </summary>
        public static readonly Color Blue = Color.FromArgb(0, 0, 255);

        /// <summary>
        /// The color Brown - RGB(165, 42, 42)
        /// </summary>
        public static readonly Color Brown = Color.FromArgb(165, 42, 42);

        /// <summary>
        /// The color Light Blue - RGB(115, 115, 255)
        /// </summary>
        public static readonly Color LightBlue = Color.FromArgb(115, 115, 255);

        /// <summary>
        /// The color Red - RGB(255, 0, 0)
        /// </summary>
        public static readonly Color Red = Color.FromArgb(255, 0, 0);

        /// <summary>
        /// The color Saddle Brown - RGB(139, 69, 19)
        /// </summary>
        public static readonly Color SaddleBrown = Color.FromArgb(139, 69, 19);

        public static readonly Color BlueViolet = Color.BlueViolet;

        public static readonly Color Unexplored = Color.FromArgb(40, 40, 40);
    }


}