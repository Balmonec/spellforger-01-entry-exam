﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CavesOfTheDragonAge.Common
{
    /// <summary>
    /// A direction on the map.
    /// </summary>
    public enum Direction
    {
        /// <summary>
        /// The Direction is undefined.
        /// </summary>
        [Description("Undefined")]
        Undefined = 0,

        [Description("Current Location")]
        Here,

        [Description("North")]
        North,

        [Description("South")]
        South,

        [Description("East")]
        East,

        [Description("West")]
        West,

        [Description("North-East")]
        NorthEast,

        [Description("North-West")]
        NorthWest,

        [Description("South-East")]
        SouthEast,

        [Description("South-West")]
        SouthWest,

        [Description("Up")]
        Up,

        [Description("Down")]
        Down
    }

    /// <summary>
    /// Contains extension methods for the Direction enumeration.
    /// </summary>
    public static class DirectionExtensions
    {
        /// <summary>
        /// Gets the change in coordinates implied by this direction.
        /// </summary>
        /// <param name="direction">The direction to evaluate.</param>
        /// <returns>A Tuple indicating the change in coordinates implied by this direction.</returns>
        public static Tuple<int, int> GetDeltaCoordinates(this Direction direction)
        {
            return new Tuple<int, int>(direction.GetDeltaX(), direction.GetDeltaY());
        }

        /// <summary>
        /// Gets the change in the X coordinate implied by this direction.
        /// </summary>
        /// <param name="direction">The direction to evaluate.</param>
        /// <returns>The change in the X coordinate implied by this direction.</returns>
        public static int GetDeltaX(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                case Direction.South:
                    return 0;

                case Direction.East:
                case Direction.NorthEast:
                case Direction.SouthEast:
                    return 1;

                case Direction.West:
                case Direction.NorthWest:
                case Direction.SouthWest:
                    return -1;

                case Direction.Here:
                case Direction.Up:
                case Direction.Down:
                    return 0;

                case Direction.Undefined:
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Gets the change in the Y coordinate implied by this direction.
        /// </summary>
        /// <param name="direction">The direction to evaluate.</param>
        /// <returns>The change in the Y coordinate implied by this direction.</returns>
        public static int GetDeltaY(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                case Direction.NorthEast:
                case Direction.NorthWest:
                    return -1;

                case Direction.South:
                case Direction.SouthEast:
                case Direction.SouthWest:
                    return 1;

                case Direction.East:
                case Direction.West:
                    return 0;

                case Direction.Here:
                case Direction.Up:
                case Direction.Down:
                    return 0;

                case Direction.Undefined:
                default:
                    return 0;
            }
        }

        /// <summary>
        /// Returns true if the direction is a horizontal movement (North, South, East, West, NorthEast, NorthWest, SouthEast, or SouthWest), and false otherwise.
        /// </summary>
        /// <param name="direction">The direction to evaluate.</param>
        /// <returns>True if the direction is a horizontal movement (North, South, East, West, NorthEast, NorthWest, SouthEast, or SouthWest), and false otherwise.</returns>
        public static bool IsHorizontalMovement(this Direction direction)
        {
            return new List<Direction> { Direction.North, Direction.South, Direction.East, Direction.West, Direction.NorthEast, Direction.NorthWest, Direction.SouthEast, Direction.SouthWest }.Contains(direction);
        }

        /// <summary>
        /// Returns true if the direction is an orthogonal movement (North, South, East, West), and false otherwise.
        /// </summary>
        /// <param name="direction">The direction to evaluate.</param>
        /// <returns>True if the direction is an orthogonal movement (North, South, East, West), and false otherwise.</returns>
        public static bool IsOrthogonalMovement(this Direction direction)
        {
            return new List<Direction> { Direction.North, Direction.South, Direction.East, Direction.West }.Contains(direction);
        }

        /// <summary>
        /// Returns true if the direction is a diagonal movement (NorthEast, NorthWest, SouthEast, or SouthWest), and false otherwise.
        /// </summary>
        /// <param name="direction">The direction to evaluate.</param>
        /// <returns>True if the direction is a diagonal movement (NorthEast, NorthWest, SouthEast, or SouthWest), and false otherwise.</returns>
        public static bool IsDiagonalMovement(this Direction direction)
        {
            return new List<Direction> { Direction.NorthEast, Direction.NorthWest, Direction.SouthEast, Direction.SouthWest }.Contains(direction);
        }

        /// <summary>
        /// Returns the direction 90 degrees to the left of the given direction.
        /// </summary>
        /// <returns>The direction 90 degrees to the left of the given direction</returns>
        public static Direction Left90(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return Direction.West;

                case Direction.South:
                    return Direction.East;

                case Direction.East:
                    return Direction.North;

                case Direction.West:
                    return Direction.South;

                case Direction.NorthEast:
                    return Direction.NorthWest;

                case Direction.NorthWest:
                    return Direction.SouthWest;

                case Direction.SouthEast:
                    return Direction.NorthEast;

                case Direction.SouthWest:
                    return Direction.SouthEast;

                case Direction.Undefined:
                case Direction.Here:
                case Direction.Up:
                case Direction.Down:
                default:
                    return Direction.Undefined;
            }
        }

        /// <summary>
        /// Returns the direction 45 degrees to the left of the given direction.
        /// </summary>
        /// <returns>The direction 45 degrees to the left of the given direction</returns>
        public static Direction Left45(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return Direction.NorthWest;

                case Direction.South:
                    return Direction.SouthEast;

                case Direction.East:
                    return Direction.NorthEast;

                case Direction.West:
                    return Direction.SouthWest;

                case Direction.NorthEast:
                    return Direction.North;

                case Direction.NorthWest:
                    return Direction.West;

                case Direction.SouthEast:
                    return Direction.East;

                case Direction.SouthWest:
                    return Direction.South;

                case Direction.Undefined:
                case Direction.Here:
                case Direction.Up:
                case Direction.Down:
                default:
                    return Direction.Undefined;
            }
        }

        /// <summary>
        /// Returns the opposite of the given direction.
        /// </summary>
        /// <returns>The opposite of the given direction</returns>
        public static Direction Opposite(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return Direction.South;

                case Direction.South:
                    return Direction.North;

                case Direction.East:
                    return Direction.West;

                case Direction.West:
                    return Direction.East;

                case Direction.NorthEast:
                    return Direction.SouthWest;

                case Direction.NorthWest:
                    return Direction.SouthEast;

                case Direction.SouthEast:
                    return Direction.NorthWest;

                case Direction.SouthWest:
                    return Direction.NorthEast;

                case Direction.Up:
                    return Direction.Down;

                case Direction.Down:
                    return Direction.Up;

                case Direction.Undefined:
                case Direction.Here:
                default:
                    return Direction.Undefined;
            }
        }

        /// <summary>
        /// Returns the direction 90 degrees to the right of the given direction.
        /// </summary>
        /// <returns>The direction 90 degrees to the right of the given direction</returns>
        public static Direction Right90(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return Direction.East;

                case Direction.South:
                    return Direction.West;

                case Direction.East:
                    return Direction.South;

                case Direction.West:
                    return Direction.North;

                case Direction.NorthEast:
                    return Direction.SouthEast;

                case Direction.NorthWest:
                    return Direction.NorthEast;

                case Direction.SouthEast:
                    return Direction.SouthWest;

                case Direction.SouthWest:
                    return Direction.NorthWest;

                case Direction.Undefined:
                case Direction.Here:
                case Direction.Up:
                case Direction.Down:
                default:
                    return Direction.Undefined;
            }
        }

        /// <summary>
        /// Returns the direction 45 degrees to the right of the given direction.
        /// </summary>
        /// <returns>The direction 45 degrees to the right of the given direction</returns>
        public static Direction Right45(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return Direction.NorthEast;

                case Direction.South:
                    return Direction.SouthWest;

                case Direction.East:
                    return Direction.SouthEast;

                case Direction.West:
                    return Direction.NorthWest;

                case Direction.NorthEast:
                    return Direction.East;

                case Direction.NorthWest:
                    return Direction.North;

                case Direction.SouthEast:
                    return Direction.South;

                case Direction.SouthWest:
                    return Direction.West;

                case Direction.Undefined:
                case Direction.Here:
                case Direction.Up:
                case Direction.Down:
                default:
                    return Direction.Undefined;
            }
        }
    }
}