﻿using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures.AI;
using CavesOfTheDragonAge.Engine.MagicSpells;
using System.Collections.Generic;
using System.Linq;

namespace CavesOfTheDragonAge.Engine.Creatures
{
    /// <summary>
    /// A creature to inhabit the dungeon.
    /// </summary>
    public class Creature
    {
        #region [-- Public Methods --]

        /// <summary>
        /// A default creature.
        /// </summary>
        public Creature()
        {
            Identifier = "";
            Name = "";
            Appearance = new DisplayCharacter();
        }

        /// <summary>
        /// Take a round of actions.
        /// </summary>
        public void TakeTurn()
        {
            if (Health > 0 || Identifier == "PLAYER")
            {
                CreatureAction action = Ai.GetNextAction();
                doAction(action);
                //TODO: replace with a call to just update the faction maps for the creature's faction.
                if (Identifier == "PLAYER")
                {
                    GameData.DungeonMaps[GameData.CurrentDungeon].UpdateDijkstraMaps();
                }
            }
        }

        /// <summary>
        /// Call for the player when they die, to end the game.
        /// </summary>
        public void OnDeath()
        {
            if (Health == 0)
            {
                GameData.DungeonMaps[GameData.CurrentDungeon].KillCreature(Identifier);
            }
        }

        /// <summary>
        /// Teach the creature the given spell, if they don't already know it.
        /// </summary>
        /// <param name="spell">The MagicSpell for them to learn.</param>
        public void LearnSpell(MagicSpell spell)
        {
            if (SpellsKnown.Any(knownSpell => knownSpell == spell) == false)
            {
                SpellsKnown.Add(new MagicSpell(spell));
                Score += NEW_SPELL_SCORE;
                if (Identifier == "PLAYER")
                {
                    GameData.Ui.DisplayMessage("You have learned the spell \'" + spell.Name + "\'!", GameColors.BlueViolet);
                }
                LearnRunes();
            }
            else
            {
                if (Identifier == "PLAYER")
                {
                    GameData.Ui.DisplayMessage("You already know the spell \'" + spell.Name + "\'.");
                }
            }
        }

        /// <summary>
        /// Check all spells known by the creature, and try to find any additional runes to learn.
        /// </summary>
        public void LearnRunes()
        {
            bool learnedRune = true;

            while(learnedRune)
            {
                learnedRune = false;

                foreach(MagicSpell spell in SpellsKnown)
                {
                    bool targetRuneKnown = TargetRunesKnown.Any(rune => rune == spell.TargetRune);
                    bool areaRuneKnown = AreaRunesKnown.Any(rune => rune == spell.AreaRune);
                    bool effectRuneKnown = EffectRunesKnown.Any(rune => rune == spell.EffectRune);

                    if(targetRuneKnown && areaRuneKnown && effectRuneKnown)
                    {
                        //You know all these runes already
                        continue;
                    }

                    //If you know all the runes in a particular spell except for one, you can automatically learn the last rune.
                    #region Runes In One Spell

                    if (!targetRuneKnown && areaRuneKnown && effectRuneKnown)
                    {
                        TargetRunesKnown.Add(new TargetRune(spell.TargetRune));
                        Score += NEW_RUNE_SCORE;
                        if (Identifier == "PLAYER")
                        {
                            GameData.Ui.DisplayMessage("You have learned the \'" + spell.TargetRune.Name + "\' rune from the \'" + spell.Name + "\' spell!", GameColors.BlueViolet);
                        }
                        learnedRune = true;
                        targetRuneKnown = true;
                    }
                    else if (targetRuneKnown && !areaRuneKnown && effectRuneKnown)
                    {
                        AreaRunesKnown.Add(new AreaRune(spell.AreaRune));
                        Score += NEW_RUNE_SCORE;
                        if (Identifier == "PLAYER")
                        {
                            GameData.Ui.DisplayMessage("You have learned the \'" + spell.AreaRune.Name + "\' rune from the \'" + spell.Name + "\' spell!", GameColors.BlueViolet);
                        }
                        learnedRune = true;
                        areaRuneKnown = true;
                    }
                    else if (targetRuneKnown && areaRuneKnown && !effectRuneKnown)
                    {
                        EffectRunesKnown.Add(new EffectRune(spell.EffectRune));
                        Score += NEW_RUNE_SCORE;
                        if (Identifier == "PLAYER")
                        {
                            GameData.Ui.DisplayMessage("You have learned the \'" + spell.EffectRune.Name + "\' rune from the \'" + spell.Name + "\' spell!", GameColors.BlueViolet);
                        }
                        learnedRune = true;
                        effectRuneKnown = true;
                    }

                    #endregion Runes In One Spell

                    foreach (MagicSpell otherSpell in SpellsKnown)
                    {
                        if(spell == otherSpell)
                        {
                            continue;
                        }

                        //If an unknown rune is the only rune in common between two spells, you can learn it.
                        #region Only Rune In Common

                        if (targetRuneKnown == false && spell.TargetRune == otherSpell.TargetRune && spell.AreaRune != otherSpell.AreaRune && spell.EffectRune != otherSpell.EffectRune)
                        {
                            TargetRunesKnown.Add(new TargetRune(spell.TargetRune));
                            Score += NEW_RUNE_SCORE;
                            if (Identifier == "PLAYER")
                            {
                                GameData.Ui.DisplayMessage("You have learned the \'" + spell.TargetRune.Name + "\' rune from comparing the \'" + spell.Name
                                                           + "\' spell and the \'" + otherSpell.Name + "\' spell!", GameColors.BlueViolet);
                            }
                            learnedRune = true;
                            targetRuneKnown = true;
                        }
                        else if (areaRuneKnown == false && spell.TargetRune != otherSpell.TargetRune && spell.AreaRune == otherSpell.AreaRune && spell.EffectRune != otherSpell.EffectRune)
                        {
                            AreaRunesKnown.Add(new AreaRune(spell.AreaRune));
                            Score += NEW_RUNE_SCORE;
                            if (Identifier == "PLAYER")
                            {
                                GameData.Ui.DisplayMessage("You have learned the \'" + spell.AreaRune.Name + "\' rune from comparing the \'" + spell.Name
                                                           + "\' spell and the \'" + otherSpell.Name + "\' spell!", GameColors.BlueViolet);
                            }
                            learnedRune = true;
                            areaRuneKnown = true;
                        }
                        else if (effectRuneKnown==false && spell.TargetRune != otherSpell.TargetRune && spell.AreaRune != otherSpell.AreaRune && spell.EffectRune == otherSpell.EffectRune)
                        {
                            EffectRunesKnown.Add(new EffectRune(spell.EffectRune));
                            Score += NEW_RUNE_SCORE;
                            if (Identifier == "PLAYER")
                            {
                                GameData.Ui.DisplayMessage("You have learned the \'" + spell.EffectRune.Name + "\' rune from comparing the \'" + spell.Name
                                                           + "\' spell and the \'" + otherSpell.Name + "\' spell!", GameColors.BlueViolet);
                            }
                            learnedRune = true;
                            effectRuneKnown = true;
                        }

                        #endregion Only Rune In Common

                        //If an unknown rune is the only rune not in common between two spells, you can learn it.
                        #region Only Rune Not In Common

                        if (targetRuneKnown == false && spell.TargetRune != otherSpell.TargetRune && spell.AreaRune == otherSpell.AreaRune && spell.EffectRune == otherSpell.EffectRune)
                        {
                            TargetRunesKnown.Add(new TargetRune(spell.TargetRune));
                            Score += NEW_RUNE_SCORE;
                            if (Identifier == "PLAYER")
                            {
                                GameData.Ui.DisplayMessage("You have learned the \'" + spell.TargetRune.Name + "\' rune from comparing the \'" + spell.Name
                                                           + "\' spell and the \'" + otherSpell.Name + "\' spell!", GameColors.BlueViolet);
                            }
                            learnedRune = true;
                            targetRuneKnown = true;
                        }
                        else if (areaRuneKnown == false && spell.TargetRune == otherSpell.TargetRune && spell.AreaRune != otherSpell.AreaRune && spell.EffectRune == otherSpell.EffectRune)
                        {
                            AreaRunesKnown.Add(new AreaRune(spell.AreaRune));
                            Score += NEW_RUNE_SCORE;
                            if (Identifier == "PLAYER")
                            {
                                GameData.Ui.DisplayMessage("You have learned the \'" + spell.AreaRune.Name + "\' rune from comparing the \'" + spell.Name
                                                           + "\' spell and the \'" + otherSpell.Name + "\' spell!", GameColors.BlueViolet);
                            }
                            learnedRune = true;
                            areaRuneKnown = true;
                        }
                        else if (effectRuneKnown == false && spell.TargetRune == otherSpell.TargetRune && spell.AreaRune == otherSpell.AreaRune && spell.EffectRune != otherSpell.EffectRune)
                        {
                            EffectRunesKnown.Add(new EffectRune(spell.EffectRune));
                            Score += NEW_RUNE_SCORE;
                            if (Identifier == "PLAYER")
                            {
                                GameData.Ui.DisplayMessage("You have learned the \'" + spell.EffectRune.Name + "\' rune from comparing the \'" + spell.Name
                                                           + "\' spell and the \'" + otherSpell.Name + "\' spell!", GameColors.BlueViolet);
                            }
                            learnedRune = true;
                            effectRuneKnown = true;
                        }

                        #endregion Only Rune Not In Common
                    }
                }
            }
        }

        /// <summary>
        /// Overload the equals operator for the Creature type.
        /// </summary>
        /// <param name="left">The Creature on the left side of the operator.</param>
        /// <param name="right">The Creature on the right side of the operator.</param>
        /// <returns>The Boolean that is the result of the operator.</returns>
        public static bool operator ==(Creature left, Creature right)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(left, right))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)left == null) || ((object)right == null))
            {
                return false;
            }

            // Return true if the fields match:
            //TODO: Add AI comparison too.
            return left.Identifier == right.Identifier && left.Name == right.Name && left.Appearance == right.Appearance;
        }

        /// <summary>
        /// Overload the not-equals operator for the Creature type.
        /// </summary>
        /// <param name="left">The Creature on the left side of the operator.</param>
        /// <param name="right">The Creature on the right side of the operator.</param>
        /// <returns>The Boolean that is the result of the operator.</returns>
        public static bool operator !=(Creature left, Creature right)
        {
            return !(left == right);
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Perform the given action.
        /// </summary>
        /// <param name="action">The <see cref="CavesOfTheDragonAge.Common.CreatureAction"/> containing the specifics of the action to take.</param>
        private void doAction(CreatureAction action)
        {
            switch (action.ActionType)
            {
                case CreatureActionType.Move:
                    GameData.DungeonMaps[GameData.CurrentDungeon].MoveCreature(Identifier, action.ActionDirection);
                    break;

                case CreatureActionType.Open:
                    GameData.DungeonMaps[GameData.CurrentDungeon].TriggerCell(GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(Identifier).GetVector(action.ActionDirection), "OPEN");
                    break;

                case CreatureActionType.Close:
                    GameData.DungeonMaps[GameData.CurrentDungeon].TriggerCell(GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(Identifier).GetVector(action.ActionDirection), "CLOSE");
                    break;

                case CreatureActionType.Handle:
                    GameData.DungeonMaps[GameData.CurrentDungeon].TriggerCell(GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(Identifier).GetVector(action.ActionDirection), "HANDLE");
                    break;

                case CreatureActionType.Attack:
                    GameData.DungeonMaps[GameData.CurrentDungeon].Attack(Identifier, action.ActionDirection);
                    break;

                case CreatureActionType.Cast:
                    GameData.DungeonMaps[GameData.CurrentDungeon].CastMagicSpell(SpellsKnown[action.ActionMagnitude], action.ActionLocation, GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(Identifier), this);
                    break;

                default:
                    break;
            }
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// The identifier of the creature.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// The proper name of the creature.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The AI object controlling the creature.
        /// </summary>
        public ICreatureAI Ai { get; set; }

        /// <summary>
        /// The appearance of the creature on the screen.
        /// </summary>
        public DisplayCharacter Appearance { get; set; }

        /// <summary>
        /// The health of the creature.
        /// </summary>
        public StatPoint Health //{ get; set; }
        {
            get { return _health; }
            set
            {
                _health = value;
                if (_health == 0)
                {
                    OnDeath();
                }
            }
        }

        private StatPoint _health;

        /// <summary>
        /// The list of spells known by the creature.
        /// </summary>
        public List<MagicSpell> SpellsKnown = new List<MagicSpell>();

        /// <summary>
        /// The list of TargetRunes known by the creature.
        /// </summary>
        public List<TargetRune> TargetRunesKnown = new List<TargetRune>();

        /// <summary>
        /// The list of AreaRunes known by the creature.
        /// </summary>
        public List<AreaRune> AreaRunesKnown = new List<AreaRune>();

        /// <summary>
        /// The list of EffectRunes known by the creature.
        /// </summary>
        public List<EffectRune> EffectRunesKnown = new List<EffectRune>();

        public int Score = 0;
        private const int NEW_SPELL_SCORE = 10;
        private const int NEW_RUNE_SCORE = 10;

        //public List<string> Attributes
        //{
        //    get { return _attributes; }
        //    set { _attributes = value; }
        //}

        //private List<string> _attributes;

        #endregion [-- Properties --]
    }
}