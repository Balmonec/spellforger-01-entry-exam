﻿using System.Collections.Generic;
using System.Drawing;
using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.Engine.Creatures.AI
{
    /// <summary>
    /// A class to allow the user to control the player's actions.
    /// </summary>
    public class PlayerAi : ICreatureAI
    {
        public PlayerAi()
        {
            CurrentAutomation = new PlayerAutomation();
        }

        #region [-- Public Methods --]

        /// <summary>
        /// Determine the next action for the player to take.
        /// </summary>
        /// <returns>
        /// A CreatureAction containing the instructions for the
        /// next action for the player to take.
        /// </returns>
        public CreatureAction GetNextAction()
        {
            GameData.Ui.RenderAll();

            if (CurrentAutomation.AutomationType != PlayerAutomationType.None)
            {
                System.Threading.Thread.Sleep(GameData.AutomationWaitTime);
            }

            if (CurrentAutomation.AutomationType != PlayerAutomationType.None && cancelCurrentAutomation() == true)
            {
                CurrentAutomation = new PlayerAutomation() { AutomationType = PlayerAutomationType.None };
            }

            CreatureAction action;

            switch (CurrentAutomation.AutomationType)
            {
                case PlayerAutomationType.None:
                    action = getActionFromUi();
                    break;

                case PlayerAutomationType.Walking:
                    action = getActionWhenWalking();
                    break;

                case PlayerAutomationType.Waiting:
                    action = getActionWhenWaiting();
                    break;

                default:
                    action = GameData.Ui.GetPlayerAction();
                    break;
            }

            if (action.Validate() == false)
            {
                GameData.Ui.DisplayMessage("Invalid Action!", Color.Red);
                action = GetNextAction();
            }

            return action;
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Determine if the current PlayerAutomation needs to be canceled.
        /// </summary>
        /// <returns>Returns true if the current PlayerAutomation needs to be canceled, and false if it does not.</returns>
        private bool cancelCurrentAutomation()
        {
            if (CurrentAutomation.Validate() == false)
            {
                //The automation is not valid, and should be canceled.
                return true;
            }

            if (GameData.Ui.UserCommandWaiting == true)
            {
                //The user wants to interrupt the automation.
                return true;
            }

            if(isEnemyVisible() == true)
            {
                //An enemy is visible.
                GameData.Ui.DisplayMessage("An enemy is visible!", GameColors.Red);
                GameData.Ui.RenderAll();
                return true;
            }

            switch (CurrentAutomation.AutomationType)
            {
                case PlayerAutomationType.None:
                    return false;

                case PlayerAutomationType.Walking:
                    return cancelWalkingAutomation();

                case PlayerAutomationType.Waiting:
                    return cancelWaitingAutomation();

                default:
                    return true;
            }
        }

        private bool isEnemyVisible()
        {
            foreach(KeyValuePair<string, Creature> creature in GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList)
            {
                if (creature.Key == "PLAYER")
                    continue;

                int x = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureX(creature.Key);
                int y = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureY(creature.Key);

                if (GameData.DungeonMaps[GameData.CurrentDungeon].IsInDungeon(x, y) && GameData.DungeonMaps[GameData.CurrentDungeon].FOVState[x, y]==Dungeons.Dungeon.FOVStateType.Visible)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determine if a PlayerAutomation of type Waiting needs to be canceled.
        /// </summary>
        /// <returns>Returns true if the PlayerAutomation needs to be canceled, and false if it does not.</returns>
        private bool cancelWaitingAutomation()
        {
            if (CurrentAutomation.AutomationCounter <= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Determine if a PlayerAutomation of type Walking needs to be canceled.
        /// </summary>
        /// <returns>Returns true if the PlayerAutomation needs to be canceled, and false if it does not.</returns>
        private bool cancelWalkingAutomation()
        {
            Location playerLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(GameData.Ui.PlayerId);
            List<Direction> passableDirections = GameData.DungeonMaps[GameData.CurrentDungeon].GetPassableDirections(playerLocation, GameData.Ui.PlayerId);

            Location newLocation = playerLocation.GetVector(CurrentAutomation.AutomationDirection);

            if (GameData.DungeonMaps[GameData.CurrentDungeon].GetCreature(newLocation) != null)
            {
                return true;
            }

            //if (GameData.DungeonMaps[GameData.CurrentDungeon].IsPassable(newLocation, GameData.UI.PlayerID) == false)
            if (!passableDirections.Contains(CurrentAutomation.AutomationDirection))
            {
                //Is this the corner of a corridor?
                if (passableDirections.Count == 2)
                {
                    Direction newDirection = passableDirections.Find(x => x != CurrentAutomation.AutomationDirection.Opposite());
                    CurrentAutomation.AutomationDirection = newDirection;
                    return false;
                }
                else
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Get the action to take from the UI, interpreting as needed.
        /// </summary>
        /// <returns>
        /// A CreatureAction containing the instructions for the
        /// next action for the player to take.
        /// </returns>
        private CreatureAction getActionFromUi()
        {
            CreatureAction action = GameData.Ui.GetPlayerAction();

            switch (action.ActionType)
            {
                case CreatureActionType.Walk:
                    CurrentAutomation.AutomationType = PlayerAutomationType.Walking;
                    CurrentAutomation.AutomationDirection = action.ActionDirection;
                    action.ActionType = CreatureActionType.Move;
                    break;

                case CreatureActionType.Waiting:
                    CurrentAutomation.AutomationType = PlayerAutomationType.Waiting;
                    CurrentAutomation.AutomationCounter = 100;
                    action.ActionType = CreatureActionType.Wait;
                    break;

                default:
                    //No override needed, so do nothing.
                    break;
            }

            return action;
        }

        /// <summary>
        /// Determine the next action for a waiting player to take.
        /// </summary>
        /// <returns>
        /// A CreatureAction containing the instructions for the
        /// next action for a walking player to take.
        /// </returns>
        private CreatureAction getActionWhenWaiting()
        {
            CurrentAutomation.AutomationCounter--;
            return new CreatureAction() { ActionType = CreatureActionType.Wait };
        }

        /// <summary>
        /// Determine the next action for a walking player to take.
        /// </summary>
        /// <returns>
        /// A CreatureAction containing the instructions for the
        /// next action for a walking player to take.
        /// </returns>
        private CreatureAction getActionWhenWalking()
        {
            return new CreatureAction() { ActionType = CreatureActionType.Move, ActionDirection = CurrentAutomation.AutomationDirection };
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// Holds the appropriate information about the player's current active automation.
        /// </summary>
        public PlayerAutomation CurrentAutomation { get; set; }

        /// <summary>
        /// The creature this AI is part of.
        /// </summary>
        public Creature MyCreature { get; set; }

        #endregion [-- Properties --]
    }
}