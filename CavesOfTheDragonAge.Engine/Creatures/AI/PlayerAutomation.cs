﻿using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.Engine.Creatures.AI
{
    /// <summary>
    /// Holds the appropriate information about the player's current active automation.
    /// </summary>
    public class PlayerAutomation
    {
        public PlayerAutomation()
        {
            AutomationCounter = 0;
            AutomationDirection = Direction.Undefined;
            AutomationType = PlayerAutomationType.None;
        }

        #region [-- Public Methods --]

        /// <summary>
        /// Validates that the required information is set for the selected PlayerAutomationType.
        /// </summary>
        /// <returns>
        /// Whether the required information is set for the selected PlayerAutomationType.
        /// </returns>
        public bool Validate()
        {
            switch (AutomationType)
            {
                case PlayerAutomationType.None:
                    return true;

                case PlayerAutomationType.Walking:
                    if (AutomationDirection != Direction.Undefined && AutomationDirection != Direction.Up && AutomationDirection != Direction.Down)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case PlayerAutomationType.Waiting:
                    if (AutomationCounter < 0 || AutomationCounter == int.MaxValue)
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }

                default:
                    return false;
            }
        }

        #endregion [-- Public Methods --]



        #region [-- Properties --]

        /// <summary>
        /// Shows the number of turns the player automation should execute.
        /// </summary>
        public int AutomationCounter { get; set; }

        /// <summary>
        /// Shows the <href=Direction> of the player automation that is currently being used.</href>
        /// </summary>
        public Direction AutomationDirection { get; set; }

        /// <summary>
        /// Shows the type of player automation that is currently being used.
        /// </summary>
        public PlayerAutomationType AutomationType { get; set; }

        #endregion [-- Properties --]
    }
}