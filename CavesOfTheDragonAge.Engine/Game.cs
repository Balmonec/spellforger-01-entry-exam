using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.Creatures;
using CavesOfTheDragonAge.Engine.Dungeons;
using System.Collections.Generic;
using System.IO;

namespace CavesOfTheDragonAge.Engine
{
    /// <summary>
    /// Game
    /// </summary>
    public class Game
    {
        /// <summary>
        /// Game
        /// </summary>
        /// <param name="userInterface"></param>
        public Game(IUserInterface userInterface)
        {
            GameData.Setup(userInterface);
        }

        public void MainMenu()
        {
            int choice = GameData.Ui.DisplayMenu("Main Menu", new List<string>() {
                                    "New Game",
                                    "Credits",
                                    "Exit" },
                                    false,
                                    0,
                                    "mainmenu.png");

            switch(choice)
            {
                case 0:
                    Run();
                    MainMenu();
                    break;

                case 1:
                    GameData.Ui.DisplayMenu(File.ReadAllText("Credits.txt"), null, false, 0, "mainmenu.png");
                    MainMenu();
                    break;

                case 2:
                    break;

                default:
                    MainMenu();
                    break;
            }
        }

        /// <summary>
        /// Run
        /// </summary>
        public void Run()
        {
            GameData.Ui.PlayerId = "PLAYER";
            GameData.DungeonMaps = new Dictionary<string, Dungeon>
            {
                {"DUNGEONLEVEL0", DungeonFactory.GetDungeon(DungeonFactory.GeneratorType.ByRandomRooms, "DUNGEONLEVEL0", new System.Tuple<int, int>(80, 40))}
            };

            GameData.CurrentDungeon = "DUNGEONLEVEL0";
            GameData.Depth = 1;
            GameData.EndGame = false;

            GameData.Ui.DisplayMenu("     You are taking the entrance exam to an EXCLUSIVE Mage Academy." + System.Environment.NewLine
                                  + System.Environment.NewLine
                                  + "To succeed, you must delve three levels deep into the dungeon, and survive." + System.Environment.NewLine
                                  + "Along the way, you will begin your education, by learning spells found in the" + System.Environment.NewLine
                                  + "dungeon, breaking them down to their component runes, and forging your own" + System.Environment.NewLine
                                  + "powerful spells.  Only the strongest potential mage will survive!" + System.Environment.NewLine
                                  + System.Environment.NewLine
                                  + "Good luck!", null, false, 0, "mainmenu.png");

            GameData.Ui.RenderAll();

            string name = GameData.Ui.GetTextFromUser("What is your name? ");
            if (!string.IsNullOrEmpty(name))
            {
                GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList[GameData.Ui.PlayerId].Name = name;
            }

            GameData.Ui.DisplayMessage("Press '?' for a help menu.", System.Drawing.Color.Yellow);

            int gameclock = 0;
            //TODO: Find a way to fix the isWindowClosed hack.
            while (!GameData.EndGame /*&& !TCODConsole.isWindowClosed()*/)
            {
                GameData.GoDown = false;
                GameData.StartOver = false;
                //GameData.Ui.DisplayMessage("Game Clock: " + gameclock);
                gameclock++;

                //GameData.Ui.DisplayMessage("Dungeon Size: "
                //                           + GameData.DungeonMaps[GameData.CurrentDungeon].Width
                //                           + " wide by "
                //                           + GameData.DungeonMaps[GameData.CurrentDungeon].Height
                //                           + " high.");

                foreach (KeyValuePair<string, Creature> creature in GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList)
                {
                    //GameData.Ui.DisplayMessage(creature.Value.Identifier + " is taking a turn.", creature.Value.Appearance.ForegroundColor);

                    creature.Value.TakeTurn();
                    if (GameData.EndGame == true)
                    {
                        break;
                    }
                }

                if (GameData.EndGame == false)
                {
                    GameData.DungeonMaps[GameData.CurrentDungeon].UpdateCells();
                }

                if (GameData.StartOver == true)
                {
                    StartOver();
                }

                if (GameData.GoDown == true)
                {
                    NextLevel();
                }
            }
        }

        public void NextLevel()
        {
            int choice = GameData.Ui.DisplayMenu("Do you wish to descend to the next level?", new List<string>() { "Yes", "No" });

            if(choice == 0)
            {
                //advance to the next level
                Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList["PLAYER"];

                GameData.Ui.DisplayMessage("You take a moment to rest, and recover your strength.", GameColors.LightBlue);
                player.Health = player.Health + (player.Health.Maximum / 2);

                GameData.Ui.DisplayMessage("After a rare moment of peace, you descend deeper into the heart of the dungeon...", GameColors.Red);
                GameData.Depth++;

                if(GameData.Depth > GameData.MaximumDepth)
                {
                    GameData.Ui.RenderAll();
                    GameData.Ui.DisplayMenu(getScoreMessage(player.Score), null);
                    GameData.EndGame = true;
                }
                else
                {
                    Location oldPlayerLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(player.Identifier);

                    string newLevelName = "DUNGEONLEVEL" + GameData.DungeonMaps.Count;
                    GameData.DungeonMaps.Add(newLevelName, DungeonFactory.GetDungeon(DungeonFactory.GeneratorType.ByRandomRooms, newLevelName, new System.Tuple<int, int>(80, 40), player));
                    GameData.CurrentDungeon = newLevelName;
                }
            }
        }

        public void StartOver()
        {
            //advance to the next level
            Creature player = GameData.DungeonMaps[GameData.CurrentDungeon].CreatureList["PLAYER"];
            GameData.Depth = 1;

            GameData.Ui.DisplayMessage("You return to try again, recovered in strength.", GameColors.LightBlue);
            player.Health.Current = player.Health.Maximum;
            player.Score = 0;

            Location oldPlayerLocation = GameData.DungeonMaps[GameData.CurrentDungeon].GetCreatureLocation(player.Identifier);

            string newLevelName = "DUNGEONLEVEL" + GameData.DungeonMaps.Count;
            GameData.DungeonMaps.Add(newLevelName, DungeonFactory.GetDungeon(DungeonFactory.GeneratorType.ByRandomRooms, newLevelName, new System.Tuple<int, int>(80, 40), player));
            GameData.CurrentDungeon = newLevelName;
        }

        private string getScoreMessage(int score)
        {
            if(score < 25)
            {
                return "Final Score: " + score + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You have been given a full ride scholarship..... at the Warrior and" + System.Environment.NewLine
                     + "Swordfighter Academy across town." + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "Please do not apply to the Wizard Academy again." + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You win?";
            }
            else if (score < 100)
            {
                return "Final Score: " + score + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You must have run away from every monster you came across to get such" + System.Environment.NewLine
                     + "a poor score.  Either you are a coward (have you considered being a" + System.Environment.NewLine
                     + "peasant), or you are very sneaky (perhaps you should consider a career" + System.Environment.NewLine
                     + "as a spy?)." + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You managed not to die, so.. you win?";
            }
            else if(score < 300)
            {
                return "Final Score: " + score + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You are grudgingly accepted into the Wizard Academy.  If you work hard" + System.Environment.NewLine
                     + "and live frugally, you might even be able to pay off your student debt" + System.Environment.NewLine
                     + "in fifty to a hundred years." + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You win.";
            }
            else if(score < 500)
            {
                return "Final Score: " + score + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You are accepted into the Wizard Academy, and given the standard" + System.Environment.NewLine
                     + "scholarships.  A (hopefully) profitable career as a Wizard awaits" + System.Environment.NewLine
                     + "you!  If you can do well on all your classes, that is..." + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You win.";
            }
            else if (score < 600)
            {
                return "Final Score: " + score + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You are gladly accepted into the Wizard Academy, and invited to join" + System.Environment.NewLine
                     + "the honors club with a substantial scholarship package waiting for" + System.Environment.NewLine
                     + "you.  We look forward to seeing how you will benefit our institution." + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You win.";
            }
            else
            {
                return "Final Score: " + score + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You passed with flying colours, and are given a full ride scholarship!" + System.Environment.NewLine
                     + "A long and profitable career as a Wizard is in your future!  If you" + System.Environment.NewLine
                     + "continue to show this much promise, you could be on the fast track to be" + System.Environment.NewLine
                     + "High Magister of the College, or at the very least an extraordinarily" + System.Environment.NewLine
                     + "generous alumnus." + System.Environment.NewLine
                     + System.Environment.NewLine
                     + "You Win!";
            }
        }
    }
}