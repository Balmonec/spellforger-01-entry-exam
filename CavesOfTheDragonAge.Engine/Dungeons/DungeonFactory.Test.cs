﻿using System;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public static partial class DungeonFactory
    {
        /// <summary>
        /// Generate a test dungeon, hardcoded to have specified features.
        /// </summary>
        /// <returns>A hardcoded Dungeon.</returns>
        private static Dungeon manualTestGenerator()
        {
            string map = @"###############################################################################" + Environment.NewLine
                       + @"#.............................o...............................................#" + Environment.NewLine
                       + @"#..........##################################################################.#" + Environment.NewLine
                       + @"#....@...............########################################################.#" + Environment.NewLine
                       + @"#..........#########.########################################################.#" + Environment.NewLine
                       + @"#..........#########.########################################################.#" + Environment.NewLine
                       + @"#.##################.########################################################.#" + Environment.NewLine
                       + @"#.##################.########################################################.#" + Environment.NewLine
                       + @"#.##################.########################################################.#" + Environment.NewLine
                       + @"#.##################.########################################################.#" + Environment.NewLine
                       + @"#.##########....o.....#######################################################.#" + Environment.NewLine
                       + @"#.##########..........#######################################################.#" + Environment.NewLine
                       + @"#.##########.....o....#######################################################.#" + Environment.NewLine
                       + @"#.##########...o......#######################################################.#" + Environment.NewLine
                       + @"#.##########..........#######################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#o###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.###########################################################################.#" + Environment.NewLine
                       + @"#.............................................................................#" + Environment.NewLine
                       + @"###############################################################################";

            //string map = @"###==*==###########################################################################################################" + Environment.NewLine
            //           + @"###=====###########################################################################################################" + Environment.NewLine
            //           + @"###=====#####1111#######...............######################S.S.S#################################################" + Environment.NewLine
            //           + @"###======####1111#######...............#####################.......################################################" + Environment.NewLine
            //           + @"###======####1111#######...............#22222##############S.......S###############################################" + Environment.NewLine
            //           + @"###======####1111#######.........@o....#2#2#2##############.........###############################################" + Environment.NewLine
            //           + @"####======##############...............#22222#############....._.....##############################################" + Environment.NewLine
            //           + @"####=======#############...............#2#2#2###########...............############################################" + Environment.NewLine
            //           + @"#####=======#===============...........#22222###########...............############################################" + Environment.NewLine
            //           + @"######=========================........#################...............############################################" + Environment.NewLine
            //           + @"#######=========================.......#################..##.......##..+......#####################################" + Environment.NewLine
            //           + @"########======....===============......#################..##.......##..####...#####################################" + Environment.NewLine
            //           + @"#......bbbbbb......===============.....#################...............#..+...#####################################" + Environment.NewLine
            //           + @"#.#####======......===============.....#################...............#..#...#####################################" + Environment.NewLine
            //           + @"#.#...#======......===============.....+......##########..##.......##..#..###X#####################################" + Environment.NewLine
            //           + @"#.#.#.#======......===============.....######.##########..##.......##..#..###.#####################################" + Environment.NewLine
            //           + @"#.#.#.##======....===============......######.##########...............######.#####################################" + Environment.NewLine
            //           + @"#...#.###=======================.......######.##########...............####...#####################################" + Environment.NewLine
            //           + @"#####.####=====================........######.##########..##.......##..####.###....########################......##" + Environment.NewLine
            //           + @"#####.#######===============.......o...######.X........X..##.......##..####.###.##X########################......##" + Environment.NewLine
            //           + @"#####.############======...............######.##########...............####.....##.........######....######......##" + Environment.NewLine
            //           + @"#####....#########======...............######.##########...............###########.........######....######......##" + Environment.NewLine
            //           + @"########.#########======...............######.##########..##.......##..###########........./....+...,---###......##" + Environment.NewLine
            //           + @"########.#########======...............######.##########..##.......##..###########.........######....##-###......##" + Environment.NewLine
            //           + @"########.#########======...............######.##########...............###########.........######....##-#####..####" + Environment.NewLine
            //           + @"########..ooo....#======...............######.##########...............###########.........############------||####" + Environment.NewLine
            //           + @"################.#=======..............######.##########..##.......##..###########.........##################..####" + Environment.NewLine
            //           + @"################.#=======####################.##########..##.......##..###########.........##################..####" + Environment.NewLine
            //           + @"################.##======####################.##########...............###########.........##################..####" + Environment.NewLine
            //           + @"################.##======####################.##########...............###########.........##################..####" + Environment.NewLine
            //           + @"################.##======####################.#################+##################.........##################..####" + Environment.NewLine
            //           + @"################.##======####################.#################.##################.........##################..####" + Environment.NewLine
            //           + @"#####........###.##======####################.#################.##################................###########..####" + Environment.NewLine
            //           + @"#####.######.###.##======####################.#################.##################................###########++####" + Environment.NewLine
            //           + @"#####.######.###.##======####################.#################.##################................#########......##" + Environment.NewLine
            //           + @"#####.######.###.##======####################.#################.##########........................#########......##" + Environment.NewLine
            //           + @"#####......#.###.##======####################.#################.##########...........'............+.......+......##" + Environment.NewLine
            //           + @"##########.#.###.##======####################+#################.##########............'...........#########......##" + Environment.NewLine
            //           + @"##########.#.....##======......###########.......##############.##########............''..........#########......##" + Environment.NewLine
            //           + @"##########.########======......###########.......##############.##########.............''.........#########......##" + Environment.NewLine
            //           + @"###........#########=====......###########.......+.......................+.............''.........#########......##" + Environment.NewLine
            //           + @"###.################======.....###########...U...#################.#######...........##'''############....+......##" + Environment.NewLine
            //           + @"###.################======.....###########.......#################.#######...........###'''###########.####......##" + Environment.NewLine
            //           + @"###.###########.....======.....###########.......#################.#######...........###''''''....####.####......##" + Environment.NewLine
            //           + @"###.###########.....======.....+.........+.......#################.#############.....##''''''''''.####.############" + Environment.NewLine
            //           + @"###.###########.....======.....###################################.#############.....#'####'''''..+....############" + Environment.NewLine
            //           + @"###.###########.....bbbbbb.....###################################.#############.....#######''''..####.############" + Environment.NewLine
            //           + @"###.###########.....bbbbbb.....###################################.#############.....######''''...####.############" + Environment.NewLine
            //           + @"###...........+.....======.....###################################+#############.....######''''#######.############" + Environment.NewLine
            //           + @"######.########.....======.....################################..........#######.....#######'''''#####+############" + Environment.NewLine
            //           + @"######.########.....======.....################################..........#######.....####...''''''##.....##########" + Environment.NewLine
            //           + @"######.########......======....################################..........+.....+.....####....'''''##.....##########" + Environment.NewLine
            //           + @"######.##############========..################################..........#######.....####....''''''#.....##########" + Environment.NewLine
            //           + @"######.###############===============##########################..........#######.....####....'''''''.....##########" + Environment.NewLine
            //           + @"######.###############===============================##########..........################....''''''#.....##########" + Environment.NewLine
            //           + @"######.################============================================......####################'''''######+##########" + Environment.NewLine
            //           + @"######.#################===========================================bb==========############'''''''######.......####" + Environment.NewLine
            //           + @"######.##################==========================================bb=============##########''''''##'###...''..####" + Environment.NewLine
            //           + @"######.#####################=======================================bb===============#######.''''''''####.''''..####" + Environment.NewLine
            //           + @"######+##############################==============================bb================######..''''''''##''''''..####" + Environment.NewLine
            //           + @"###........#####################################===================bb=================#####...'''''''''''''.''.####" + Environment.NewLine
            //           + @"###........###############################################=========bb==================####....'''''''''''..''.####" + Environment.NewLine
            //           + @"###........####################################################....bb==================####.....'''''''''####''####" + Environment.NewLine
            //           + @"###...<....########################...........#################..........#####=========####.......'''''''#####'####" + Environment.NewLine
            //           + @"###........########################...........#################..........#######=======####.......#''''########'###" + Environment.NewLine
            //           + @"###........########################...........#################..........####...========###.......##'##############" + Environment.NewLine
            //           + @"###........########################...........#################..........####....=======#############'#############" + Environment.NewLine
            //           + @"###........########################...........+...............+..........+..+..>.=======###########################" + Environment.NewLine
            //           + @"###################################...........#################..........####....=======###########################" + Environment.NewLine
            //           + @"###################################...........###############################....=======###########################" + Environment.NewLine
            //           + @"###################################...........####################################======###########################" + Environment.NewLine
            //           + @"##################################################################################=======##########################" + Environment.NewLine
            //           + @"##################################################################################=======##########################" + Environment.NewLine
            //           + @"##################################################################################=======##########################" + Environment.NewLine
            //           + @"##################################################################################=======##########################";
            //+ "_______####################" + Environment.NewLine
            //+ "####+_________________#####" + Environment.NewLine
            //+ "###_____________________###" + Environment.NewLine
            //+ "#__*__________________*___#" + Environment.NewLine
            //+ "#_________________________#" + Environment.NewLine
            //+ "###_____________________###" + Environment.NewLine
            //+ "#.###_________________#####" + Environment.NewLine
            //+ "#........._....@..........#" + Environment.NewLine
            //+ "#....___..____________....#" + Environment.NewLine
            //+ "#...__._....._......._....#" + Environment.NewLine
            //+ "#...._._______________....#" + Environment.NewLine
            //+ "###########################";

            Dungeon testDungeon = bySimpleMapGenerator(map);

            testDungeon.Name = "TESTING_GROUND";

            return testDungeon;
        }
    }
}