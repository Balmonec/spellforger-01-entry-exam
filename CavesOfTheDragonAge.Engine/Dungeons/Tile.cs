﻿using CavesOfTheDragonAge.Common;
using System.Collections.Generic;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    /// <summary>
    /// A single square of the dungeon terrain.
    /// </summary>
    public class Tile
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Construct a tile with default attributes.
        /// </summary>
        public Tile()
        {
            Type = "NULL";
            Appearance = new DisplayCharacter();
            Attributes = new List<string>() { "NONE" };
        }

        /// <summary>
        /// Construct a tile with attributes.
        /// </summary>
        /// <param name="type">The identifying name of the tile.</param>
        /// <param name="appearance">The appearance of the tile on the screen.</param>
        /// <param name="attributes">A list of any attributes on the tile.</param>
        public Tile(string type, DisplayCharacter appearance, bool blocksVision = false, bool blocksMovement = false, List<string> attributes = null)
        {
            Type = type;
            Appearance = appearance;
            BlocksVision = blocksVision;
            BlocksMovement = blocksMovement;
            Attributes = attributes;
        }

        /// <summary>
        /// Returns a string containing the Type of the Tile.
        /// </summary>
        /// <returns>The Type of the Tile.</returns>
        public override string ToString()
        {
            return Type;
        }

        #endregion [-- Public Methods --]

        #region [-- Properties --]

        public DisplayCharacter Appearance { get; set; }

        public List<string> Attributes { get; set; }

        public string Type { get; set; }

        public bool BlocksVision = false;
        public bool BlocksMovement = false;

        #endregion [-- Properties --]
    }
}