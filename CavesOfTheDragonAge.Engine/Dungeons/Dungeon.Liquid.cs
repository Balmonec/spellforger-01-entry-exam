﻿using System.Linq;
using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public partial class Dungeon
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Get the Liquid object for a given square.
        /// </summary>
        /// <param name="x">The X coordinate for the Liquid to return.</param>
        /// <param name="y">The Y coordinate for the Liquid to return.</param>
        /// <returns>The Liquid object in the given square.</returns>
        public Liquid GetLiquid(int x, int y)
        {
            Liquid currentLiquid;
            if (!Liquids.IndexInBounds2D(x, y) || !GameData.Liquids.TryGetValue(Liquids[x, y], out currentLiquid))
            {
                currentLiquid = new Liquid();
            }
            return currentLiquid;
        }

        /// <summary>
        /// Get the Liquid object for a given square.
        /// </summary>
        /// <param name="location">The Location the Liquid to return.</param>
        /// <returns>The Liquid object in the given square.</returns>
        public Liquid GetLiquid(Location location)
        {
            return GetLiquid(location.X, location.Y);
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Alter the adjacent Liquid tiles.
        /// </summary>
        /// <param name="x">The X coordinate of the originating tile.</param>
        /// <param name="y">The Y coordinate of the originating tile.</param>
        /// <param name="attribute">The complete attribute containing the information to change the water tile from and to.</param>
        private void alterAdjacentLiquid(int x, int y, string attribute)
        {
            for (int a = x - 1; a <= x + 1; a++)
            {
                if (a >= 0 && a < Width)
                {
                    for (int b = y - 1; b <= y + 1; b++)
                    {
                        if (b >= 0 && b < Height
                            && (a != x || b != y)
                            && attribute.Split(':')[1] == Liquids[a, b])
                        {
                            _liquidsBuffer[a, b] = attribute.Split(':')[2];
                            ImpassableCells = null;
                            TransparentCells = null;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Alter the Liquid tile immediately.
        /// </summary>
        /// <param name="x">The X coordinate of the Liquid.</param>
        /// <param name="y">The Y coordinate of the Liquid.</param>
        /// <param name="attribute">The complete attribute containing the information to change the water tile to.</param>
        private void alterSelfImmediateLiquid(int x, int y, string attribute)
        {
            alterSelfLiquid(x, y, attribute);
            Liquids[x, y] = attribute.Split(':')[1];
            ImpassableCells = null;
            TransparentCells = null;
        }

        /// <summary>
        /// Alter the Liquid tile.
        /// </summary>
        /// <param name="x">The X coordinate of the Liquid.</param>
        /// <param name="y">The Y coordinate of the Liquid.</param>
        /// <param name="attribute">The complete attribute containing the information to change the water tile to.</param>
        private void alterSelfLiquid(int x, int y, string attribute)
        {
            _liquidsBuffer[x, y] = attribute.Split(':')[1];
            ImpassableCells = null;
            TransparentCells = null;
        }

        /// <summary>
        /// Return a copy of the Liquid layer to use when updating the cells.
        /// </summary>
        /// <returns>A copy of the Liquid layer</returns>
        private string[,] copyLiquids()
        {
            string[,] newLiquids = new string[Width, Height];

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    newLiquids[x, y] = string.Copy(Liquids[x, y]);
                }
            }

            return newLiquids;
        }

        /// <summary>
        /// Determines if the Liquid in the given location will respond to the given trigger.
        /// </summary>
        /// <param name="location">The Location of the cell.</param>
        /// <param name="trigger">The trigger to check.</param>
        /// <returns>Returns TRUE if the Liquid in the given location will respond to the given trigger, and FALSE otherwise.</returns>
        private bool hasLiquidTrigger(Location location, string trigger)
        {
            return GetLiquid(location).Attributes
                .Any(attribute => attribute.StartsWith("ON_TRIGGER:" + trigger));
        }

        /// <summary>
        /// Process the attribute of a Liquid to see if it requires action.
        /// </summary>
        /// <param name="x">The X coordinate of the Liquid.</param>
        /// <param name="y">The Y coordinate of the Liquid.</param>
        /// <param name="attribute">The complete attribute.</param>
        private void processLiquidAttribute(int x, int y, string attribute)
        {
            if (attribute.Split(':')[0] == "ALTER_ADJACENT_LIQUID")
            {
                alterAdjacentLiquid(x, y, attribute);
            }
            else if (attribute.Split(':')[0] == "ALTER_SELF")
            {
                alterSelfLiquid(x, y, attribute);
            }
            else if (attribute.Split(':')[0] == "ALTER_SELF_IMMEDIATE")
            {
                alterSelfImmediateLiquid(x, y, attribute);
            }
            else if (attribute.Split(':')[0] == "TRIGGER_ADJACENT")
            {
                internalTriggerAdjacent(x, y, attribute.Split(':')[1]);
            }
            else if (attribute.Split(':')[0] == "TRIGGER_SELF")
            {
                internalTriggerCell(x, y, attribute.Split(':')[1]);
            }
            else if (attribute.Split(':')[0] == "ON_CREATURE" && Creatures[x, y] != "")
            {
                processLiquidAttribute(x, y, attribute.Split(new char[] { ':' }, 2)[1]);
            }
            else if (attribute.Split(':')[0] == "MOVE_DOWNWARD")
            {
                if (Creatures[x, y] == "PLAYER")
                {
                    GameData.GoDown = true;
                }
            }
        }

        /// <summary>
        /// Trigger an action in the selected liquid.
        /// </summary>
        /// <param name="x">The X coordinate of the liquid.</param>
        /// <param name="y">The Y coordinate of the liquid.</param>
        /// <param name="trigger">The name of the trigger.</param>
        private void triggerLiquid(int x, int y, string trigger)
        {
            foreach (string attribute in GetLiquid(x, y).Attributes)
            {
                if (attribute.StartsWith("ON_TRIGGER:" + trigger))
                {
                    processLiquidAttribute(x, y, attribute.Split(new char[] { ':' }, 3)[2]);
                }
            }
        }

        /// <summary>
        /// Cycle through the Liquid layer and update based on cell behavior.
        /// </summary>
        private void updateLiquids()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    foreach (string attribute in GetLiquid(x, y).Attributes)
                    {
                        processLiquidAttribute(x, y, attribute);
                    }
                }
            }
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// An array of strings indicating the map of Liquid objects.
        /// </summary>
        public string[,] Liquids
        {
            get { return _liquids; }
            set { _liquids = value; }
        }

        /// <summary>
        /// The internal string array holding the map of Liquid objects.
        /// </summary>
        private string[,] _liquids;

        /// <summary>
        /// The internal buffer of the liquids.
        /// </summary>
        private string[,] _liquidsBuffer;

        #endregion [-- Properties --]
    }
}