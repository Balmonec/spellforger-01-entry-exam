﻿using CavesOfTheDragonAge.Common;
using CavesOfTheDragonAge.Engine.MagicSpells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public static partial class DungeonFactory
    {
        /// <summary>
        /// Fill all tiles in the dungeon with the same tile.
        /// </summary>
        /// <param name="dungeon">This dungeon to fill.</param>
        /// <param name="tileType">The name of the tile type to fill with.</param>
        /// <param name="rect">The rectangle to fill - defaults to the entire map.</param>
        private static void fillTiles(this Dungeon dungeon, string tileType = "NULL", Rect rect = null)
        {
            if(rect==null)
            {
                rect = new Rect(0, 0, dungeon.Width, dungeon.Height);
            }

            for (int x = rect.x1; x < rect.x2; x++)
            {
                for (int y = rect.y1; y < rect.y2; y++)
                {
                    dungeon.Tiles[x, y] = tileType;
                }
            }
        }

        /// <summary>
        /// Fill all liquids in the dungeon with the same liquid.
        /// </summary>
        /// <param name="dungeon">This dungeon to fill.</param>
        /// <param name="liquidType">The name of the tile type to fill with.</param>
        /// <param name="rect">The rectangle to fill - defaults to the entire map.</param>
        private static void fillLiquids(this Dungeon dungeon, string liquidType = "NULL", Rect rect = null)
        {
            if (rect == null)
            {
                rect = new Rect(0, 0, dungeon.Width, dungeon.Height);
            }

            for (int x = rect.x1; x < rect.x2; x++)
            {
                for (int y = rect.y1; y < rect.y2; y++)
                {
                    dungeon.Liquids[x, y] = liquidType;
                }
            }
        }

        /// <summary>
        /// Fill all fixtures in the dungeon with the same fixture.
        /// </summary>
        /// <param name="dungeon">This dungeon to fill.</param>
        /// <param name="fixtureType">The name of the fixture type to fill with.</param>
        /// <param name="rect">The rectangle to fill - defaults to the entire map.</param>
        private static void fillFixtures(this Dungeon dungeon, string fixtureType = "NULL", Rect rect = null)
        {
            if (rect == null)
            {
                rect = new Rect(0, 0, dungeon.Width, dungeon.Height);
            }

            for (int x = rect.x1; x < rect.x2; x++)
            {
                for (int y = rect.y1; y < rect.y2; y++)
                {
                    dungeon.Fixtures[x, y] = fixtureType;
                }
            }
        }

        /// <summary>
        /// Fill all creatures in the dungeon with the same creature.
        /// </summary>
        /// <param name="dungeon">This dungeon to fill.</param>
        /// <param name="creatureType">The name of the creature type to fill with.</param>
        /// <param name="rect">The rectangle to fill - defaults to the entire map.</param>
        private static void fillCreatures(this Dungeon dungeon, string creatureType = "", Rect rect = null)
        {
            if (rect == null)
            {
                rect = new Rect(0, 0, dungeon.Width, dungeon.Height);
            }

            for (int x = rect.x1; x < rect.x2; x++)
            {
                for (int y = rect.y1; y < rect.y2; y++)
                {
                    dungeon.Creatures[x, y] = creatureType;
                }
            }
        }

        /// <summary>
        /// Dig a tunnel randomly from the start to the end.
        /// </summary>
        /// <param name="dungeon">This dungeon to dig in.</param>
        /// <param name="start">The coordinate of the start square.</param>
        /// <param name="end">The coordinate of the end square.</param>
        private static void digTunnel(this Dungeon dungeon, Tuple<int, int> start, Tuple<int, int> end)
        {
            if (Randomization.RandomBool())
            {
                //first move horizontally, then vertically
                int x1 = start.Item1 < end.Item1 ? start.Item1 : end.Item1;
                int x2 = start.Item1 < end.Item1 ? end.Item1 : start.Item1;
                for (int x = x1; x <= x2; x++)
                {
                    dungeon.placeWalkable(x, start.Item2);
                }

                int y1 = start.Item2 < end.Item2 ? start.Item2 : end.Item2;
                int y2 = start.Item2 < end.Item2 ? end.Item2 : start.Item2;
                for (int y = y1; y <= y2; y++)
                {
                    dungeon.placeWalkable(end.Item1, y);
                }
            }
            else
            {
                //first move horizontally, then vertically
                int y1 = start.Item2 < end.Item2 ? start.Item2 : end.Item2;
                int y2 = start.Item2 < end.Item2 ? end.Item2 : start.Item2;
                for (int y = y1; y <= y2; y++)
                {
                    dungeon.placeWalkable(start.Item1, y);
                }

                int x1 = start.Item1 < end.Item1 ? start.Item1 : end.Item1;
                int x2 = start.Item1 < end.Item1 ? end.Item1 : start.Item1;
                for (int x = x1; x <= x2; x++)
                {
                    dungeon.placeWalkable(x, end.Item2);
                }
            }
        }

        /// <summary>
        /// Place a walkable tile, either a floor in wall or a bridge over water.
        /// </summary>
        /// <param name="dungeon">This dungeon to place in.</param>
        /// <param name="x">The X-coordinate to place at.</param>
        /// <param name="y">The Y-coordinate to place at.</param>
        private static void placeWalkable(this Dungeon dungeon, int x, int y)
        {
            if (dungeon.Tiles[x, y] == "STONE_CAVITY")
            {
                //dungeon.Fixtures[x, y] = "FIXED_BRIDGE";
            }
            else
            {
                dungeon.Tiles[x, y] = "STONE_FLOOR";
            }
        }

        /// <summary>
        /// Place a Spell Rune at the given location.
        /// </summary>
        /// <param name="dungeon">This dungeon to place in.</param>
        /// <param name="x">The X-coordinate to place at.</param>
        /// <param name="y">The Y-coordinate to place at.</param>
        /// <param name="spell">THe spell to use in the Spell Rune.</param>
        private static bool tryPlaceSpellRune(this Dungeon dungeon, int x, int y, MagicSpell spell)
        {
            if(dungeon.Fixtures[x,y]!="NULL")
            {
                return false;
            }

            string runeName = "SPELLRUNE" + spell.Name;

            if (GameData.Fixtures.ContainsKey(runeName) == false)
            {
                //Need to add the rune to the fixtures list
                Fixture spellRune = new Fixture(GameData.Fixtures["SPELLRUNE"]);

                spellRune.Type = runeName;
                spellRune.Spell = spell;

                GameData.Fixtures.Add(runeName, spellRune);
            }

            dungeon.Fixtures[x, y] = runeName;

            return true;
        }

        private static void lineEdges(this Dungeon dungeon, string tileType = "NULL")
        {
            var rect = new Rect(0, 0, dungeon.Width, dungeon.Height);

            for (int x = rect.x1; x < rect.x2; x++)
            {
                for (int y = rect.y1; y < rect.y2; y++)
                {
                    if (x == 0 || y == 0 || x == dungeon.Width-1 || y == dungeon.Height-1)
                    {
                        dungeon.Tiles[x, y] = tileType;
                    }
                }
            }
        }

        class Rect
        {
            public int x1 = -1;
            public int y1 = -1;
            public int x2 = -1;
            public int y2 = -1;

            public Rect(int x, int y, int width, int height)
            {
                if(width >= 0)
                {
                    x1 = x;
                    x2 = x + width;
                }
                else
                {
                    x1 = x + width;
                    x2 = x;
                }

                if(height>=0)
                {
                    y1 = y;
                    y2 = y + height;
                }
                else
                {
                    y1 = y + height;
                    y2 = y;
                }
            }

            public int CenterX()
            {
                return (x1 + x2) / 2;
            }

            public int CenterY()
            {
                return (y1 + y2) / 2;
            }

            public Tuple<int, int> Center()
            {
                return new Tuple<int, int>(CenterX(), CenterY());
            }

            public int RandomX()
            {
                if (x1 == x2)
                {
                    return x1;
                }
                else
                {
                    return Randomization.RandomInt(x1, x2 - 1, true);
                }
            }

            public int RandomY()
            {
                if (y1 == y2)
                {
                    return y1;
                }
                else
                {
                    return Randomization.RandomInt(y1, y2 - 1, true);
                }
            }

            public Tuple<int, int> RandomInside()
            {
                return new Tuple<int, int>(RandomX(), RandomY());
            }

            public bool Intersects(Rect other)
            {
                //returns true if this rectangle intersects with another one
                return (x1 <= other.x2) && (x2 >= other.x1) && (y1 <= other.y2) && (y2 >= other.y1);
            }
        }
    }
}
