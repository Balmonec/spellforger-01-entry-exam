﻿using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public partial class Dungeon
    {
        public enum FOVStateType
        {
            Unknown,
            Unexplored,
            Explored,
            Visible
        }

        public void ClearFOV()
        {
            FOVState = new FOVStateType[Width, Height];
            FOVState.Init2D(FOVStateType.Unexplored);
            TransparentCells = null;
        }

        public bool IsTransparent(int x, int y)
        {
            if ((x < 0) || (y < 0) || (x >= Width) || (y >= Height))
            {
                return false;
            }
            else if (GetFixture(x, y).BlocksVision)
            {
                return false;
            }
            else if (GetLiquid(x, y).BlocksVision)
            {
                return false;
            }
            else if (GetTile(x, y).BlocksVision)
            {
                return false;
            }

            return true;
        }

        public void RebuildCellStates()
        {
            bool rebuildImpassable = false;
            if (ImpassableCells == null)
            {
                ImpassableCells = new bool[Width, Height];
                rebuildImpassable = true;
            }

            bool rebuildTransparent = false;
            if (TransparentCells == null)
            {
                TransparentCells = new bool[Width, Height];
                rebuildTransparent = true;
            }

            if (rebuildImpassable || rebuildTransparent)
            {
                for (int x = 0; x < Width; x++)
                {
                    for (int y = 0; y < Height; y++)
                    {
                        if (rebuildImpassable == true && IsPassable(x, y) == false)
                        {
                            ImpassableCells[x, y] = true;
                        }

                        if (rebuildTransparent == true && IsTransparent(x, y))
                        {
                            TransparentCells[x, y] = true;
                        }
                    }
                }
            }
        }

        public FOVStateType[,] FOVState;
        public bool[,] TransparentCells;
    }
}
