﻿using System.Collections.Generic;
using System.Linq;
using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAge.Engine.Dungeons
{
    public partial class Dungeon
    {
        #region [-- Public Methods --]

        /// <summary>
        /// Get the Fixture object for a given square.
        /// </summary>
        /// <param name="x">The X coordinate for the Fixture to return.</param>
        /// <param name="y">The Y coordinate for the Fixture to return.</param>
        /// <returns>The Fixture object in the given square.</returns>
        public Fixture GetFixture(int x, int y)
        {
            Fixture currentFixture;
            if (!Fixtures.IndexInBounds2D(x, y) || !GameData.Fixtures.TryGetValue(Fixtures[x, y], out currentFixture))
            {
                currentFixture = new Fixture();
            }
            return currentFixture;
        }

        /// <summary>
        /// Get the Fixture object for a given square.
        /// </summary>
        /// <param name="location">The Location for the Fixture to return.</param>
        /// <returns>The Fixture object in the given square.</returns>
        public Fixture GetFixture(Location location)
        {
            return GetFixture(location.X, location.Y);
        }

        /// <summary>
        /// Returns true if the given square could qualify to be a doorway.
        /// </summary>
        /// <param name="x">The X coordinate for the square to check.</param>
        /// <param name="y">The Y coordinate for the square to check.</param>
        /// <returns>Returns true if the given square could qualify to be a doorway.</returns>
        public bool IsPossibleDoorway(int x, int y)
        {
            if (GameData.Tiles[Tiles[x, y]].Attributes.Contains("CLASS:POSSIBLE_DOORWAY") == false)
            {
                return false;
            }

            bool orthoganalFrame = false;
            int bothDoorframe = 0;
            int bothDoorsill = 0;

            foreach (Direction direction in new List<Direction> { Direction.North, Direction.NorthEast, Direction.East, Direction.SouthEast })
            {
                Location firstLocation = new Location(x, y).GetVector(direction);
                Location secondLocation = new Location(x, y).GetVector(direction.Opposite());

                if(Tiles.IndexInBounds2D(firstLocation)==false || Tiles.IndexInBounds2D(secondLocation)==false)
                {
                    return false;
                }

                if(GameData.Tiles[Tiles[firstLocation.X, firstLocation.Y]].Attributes.Contains("CLASS:POSSIBLE_DOORFRAME")
                    && GameData.Tiles[Tiles[secondLocation.X, secondLocation.Y]].Attributes.Contains("CLASS:POSSIBLE_DOORFRAME"))
                {
                    bothDoorframe++;
                    if (direction == Direction.North || direction == Direction.East)
                    {
                        orthoganalFrame = true;
                    }
                }

                if (GameData.Tiles[Tiles[firstLocation.X, firstLocation.Y]].Attributes.Contains("CLASS:POSSIBLE_DOORSILL")
                    && GameData.Tiles[Tiles[secondLocation.X, secondLocation.Y]].Attributes.Contains("CLASS:POSSIBLE_DOORSILL"))
                {
                    bothDoorsill++;
                }
            }

            if (bothDoorframe >= 1 && bothDoorframe < 3 && bothDoorsill >= 1 && orthoganalFrame == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Returns true if the given square could qualify to be a doorway.
        /// </summary>
        /// <param name="location">The Location for the square to check.</param>
        /// <returns>Returns true if the given square could qualify to be a doorway.</returns>
        public bool IsPossibleDoorway(Location location)
        {
            return IsPossibleDoorway(location.X, location.Y);
        }

        #endregion [-- Public Methods --]

        #region [-- Private Methods --]

        /// <summary>
        /// Alter the adjacent Fixture tiles.
        /// </summary>
        /// <param name="x">The X coordinate of the originating tile.</param>
        /// <param name="y">The Y coordinate of the originating tile.</param>
        /// <param name="attribute">The complete attribute containing the information to change the water tile from and to.</param>
        private void alterAdjacentFixture(int x, int y, string attribute)
        {
            for (int a = x - 1; a <= x + 1; a++)
            {
                if (a >= 0 && a < Width)
                {
                    for (int b = y - 1; b <= y + 1; b++)
                    {
                        if (b >= 0 && b < Height
                            && (a != x || b != y)
                            && attribute.Split(':')[1] == Fixtures[a, b])
                        {
                            _fixturesBuffer[a, b] = attribute.Split(':')[2];
                            ImpassableCells = null;
                            TransparentCells = null;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Alter the Fixture tile.
        /// </summary>
        /// <param name="x">The X coordinate of the Fixture.</param>
        /// <param name="y">The Y coordinate of the Fixture.</param>
        /// <param name="attribute">The complete attribute containing the information to change the water tile to.</param>
        private void alterSelfFixture(int x, int y, string attribute)
        {
            _fixturesBuffer[x, y] = attribute.Split(':')[1];
            ImpassableCells = null;
            TransparentCells = null;
        }

        /// <summary>
        /// Alter the Fixture tile immediately.
        /// </summary>
        /// <param name="x">The X coordinate of the Fixture.</param>
        /// <param name="y">The Y coordinate of the Fixture.</param>
        /// <param name="attribute">The complete attribute containing the information to change the water tile to.</param>
        private void alterSelfImmediateFixture(int x, int y, string attribute)
        {
            alterSelfFixture(x, y, attribute);
            Fixtures[x, y] = attribute.Split(':')[1];
            ImpassableCells = null;
            TransparentCells = null;
        }

        /// <summary>
        /// Return a copy of the Fixture layer to use when updating the cells.
        /// </summary>
        /// <returns>A copy of the Fixture layer</returns>
        private string[,] copyFixtures()
        {
            string[,] newFixtures = new string[Width, Height];

            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    newFixtures[x, y] = string.Copy(Fixtures[x, y]);
                }
            }

            return newFixtures;
        }

        /// <summary>
        /// Determines if the Fixture in the given location will respond to the given trigger.
        /// </summary>
        /// <param name="location">The Location of the cell.</param>
        /// <param name="trigger">The trigger to check.</param>
        /// <returns>Returns TRUE if the Fixture in the given location will respond to the given trigger, and FALSE otherwise.</returns>
        private bool hasFixtureTrigger(Location location, string trigger)
        {
            return GetFixture(location).Attributes
                .Any(attribute => attribute.StartsWith("ON_TRIGGER:" + trigger));
        }

        /// <summary>
        /// Process the attribute of a Fixture to see if it requires action.
        /// </summary>
        /// <param name="x">The X coordinate of the Fixture.</param>
        /// <param name="y">The Y coordinate of the Fixture.</param>
        /// <param name="attribute">The complete attribute.</param>
        private void processFixtureAttribute(int x, int y, string attribute)
        {
            if (attribute.Split(':')[0] == "ALTER_ADJACENT_FIXTURE")
            {
                alterAdjacentFixture(x, y, attribute);
            }
            else if (attribute.Split(':')[0] == "ALTER_SELF")
            {
                alterSelfFixture(x, y, attribute);
            }
            else if (attribute.Split(':')[0] == "ALTER_SELF_IMMEDIATE")
            {
                alterSelfImmediateFixture(x, y, attribute);
            }
            else if (attribute.Split(':')[0] == "TRIGGER_ADJACENT")
            {
                internalTriggerAdjacent(x, y, attribute.Split(':')[1]);
            }
            else if (attribute.Split(':')[0] == "TRIGGER_SELF")
            {
                internalTriggerCell(x, y, attribute.Split(':')[1]);
            }
            else if (attribute.Split(':')[0] == "ON_CREATURE" && Creatures[x, y] != "")
            {
                processFixtureAttribute(x, y, attribute.Split(new char[] { ':' }, 2)[1]);
            }
            else if (attribute.Split(':')[0] == "TEACH_SPELL" && Creatures[x, y] != "")
            {
                GetCreature(x, y).LearnSpell(GetFixture(x, y).Spell);
            }
            else if (attribute.Split(':')[0] == "MOVE_DOWNWARD")
            {
                if (Creatures[x, y] == "PLAYER")
                {
                    GameData.GoDown = true;
                }
            }
        }

        /// <summary>
        /// Trigger an action in the selected Fixture.
        /// </summary>
        /// <param name="x">The X coordinate of the Fixture.</param>
        /// <param name="y">The Y coordinate of the Fixture.</param>
        /// <param name="trigger">The name of the trigger.</param>
        private void triggerFixture(int x, int y, string trigger)
        {
            foreach (string attribute in GetFixture(x, y).Attributes)
            {
                if (attribute.StartsWith("ON_TRIGGER:" + trigger))
                {
                    processFixtureAttribute(x, y, attribute.Split(new char[] { ':' }, 3)[2]);
                }
            }
        }

        /// <summary>
        /// Cycle through the Fixture layer and update based on cell behavior.
        /// </summary>
        private void updateFixtures()
        {
            for (int x = 0; x < Width; x++)
            {
                for (int y = 0; y < Height; y++)
                {
                    foreach (string attribute in GetFixture(x, y).Attributes)
                    {
                        processFixtureAttribute(x, y, attribute);
                    }
                }
            }
        }

        #endregion [-- Private Methods --]

        #region [-- Properties --]

        /// <summary>
        /// An array of strings indicating the map of Fixture objects.
        /// </summary>
        public string[,] Fixtures
        {
            get { return _fixtures; }
            set { _fixtures = value; }
        }

        /// <summary>
        /// The internal string array holding the map of Fixture objects.
        /// </summary>
        private string[,] _fixtures;

        /// <summary>
        /// The internal buffer of the Fixtures.
        /// </summary>
        private string[,] _fixturesBuffer;

        #endregion [-- Properties --]
    }
}