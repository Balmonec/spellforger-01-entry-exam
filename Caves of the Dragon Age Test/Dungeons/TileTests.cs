﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using CavesOfTheDragonAge.Common;
using System.Drawing;

namespace CavesOfTheDragonAge.Engine.Dungeons.Tests
{
    [TestClass()]
    public class TileTests
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        //
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion Additional test attributes

        /// <summary>
        ///A test for Appearance
        ///</summary>
        [TestMethod()]
        public void AppearanceTest()
        {
            Tile target = new Tile();
            DisplayCharacter expected = new DisplayCharacter('T', Color.White, Color.Black);
            target.Appearance = expected;
            DisplayCharacter actual = target.Appearance;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Attributes
        ///</summary>
        [TestMethod()]
        public void AttributesTest()
        {
            Tile target = new Tile();
            List<string> expected = new List<string>() { "Test", "This", "Function" };
            target.Attributes = expected;
            List<string> actual = target.Attributes;
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for ToString
        ///</summary>
        [TestMethod()]
        public void ToStringTest()
        {
            Tile target = new Tile();
            string expected = "Test";
            target.Type = expected;
            string actual = target.ToString();
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Type
        ///</summary>
        [TestMethod()]
        public void TypeTest()
        {
            Tile target = new Tile();
            string expected = "Test";
            target.Type = expected;
            string actual = target.Type;
            Assert.AreEqual(expected, actual);
        }
    }
}