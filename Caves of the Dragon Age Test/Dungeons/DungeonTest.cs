﻿using CavesOfTheDragonAge.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using CavesOfTheDragonAge.Engine;
using CavesOfTheDragonAge.Engine.Creatures;
using CavesOfTheDragonAge.Engine.Dungeons;

namespace CavesOfTheDragonAgeTests
{
    /// <summary>
    ///This is a test class for Dungeon and is intended
    ///to contain all Dungeon Unit Tests
    ///</summary>
    [TestClass]
    public class DungeonTest
    {
        //
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            GameData.Setup(new UserInterfaceStub());
        }

        /// <summary>
        ///A test for CreatureList
        ///</summary>
        [TestMethod]
        public void CreatureListTest()
        {
            Dungeon target = new Dungeon(); // TODO: Initialize to an appropriate value
            Dictionary<string, Creature> expected = null; // TODO: Initialize to an appropriate value
            target.CreatureList = expected;
            Dictionary<string, Creature> actual = target.CreatureList;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Creatures
        ///</summary>
        [TestMethod]
        public void CreaturesTest()
        {
            Dungeon target = new Dungeon(); // TODO: Initialize to an appropriate value
            string[,] expected = null; // TODO: Initialize to an appropriate value
            target.Creatures = expected;
            string[,] actual = target.Creatures;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Dungeon Constructor
        ///</summary>
        [TestMethod]
        public void DungeonConstructorTest()
        {
            Dungeon target = new Dungeon();
            Assert.AreEqual((target != null), true);
        }

        /// <summary>
        ///A test for getCreatureLocation
        ///</summary>
        [TestMethod]
        public void GetCreatureLocationTest()
        {
            var sampleData = new[]
            {
                new { Map = "@", Location = new Location(0, 0)},
                new { Map = ".@.", Location = new Location(1, 0)},
                new { Map = "." + Environment.NewLine
                          + "@" + Environment.NewLine
                          + ".", Location = new Location(0, 1)},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", Location = new Location(1, 1)},
            };
            foreach (var item in sampleData)
            {
                Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(item.Map);
                Location expected = item.Location;

                Location actual = testDungeon.GetCreatureLocation(GameData.Ui.PlayerId);

                Assert.AreEqual(expected, actual, "Map: " + item.Map);
            }
        }

        /// <summary>
        ///A test for getCreatureX
        ///</summary>
        [TestMethod]
        public void GetCreatureXTest()
        {
            var sampleData = new[]
            {
                new { Map = "@", X = 0},
                new { Map = ".@.", X = 1},
                new { Map = "." + Environment.NewLine
                          + "@" + Environment.NewLine
                          + ".", X = 0},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", X = 1},
            };
            foreach (var item in sampleData)
            {
                Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(item.Map);
                int expected = item.X;

                int actual = testDungeon.GetCreatureX(GameData.Ui.PlayerId);

                Assert.AreEqual(expected, actual, "Map: " + item.Map);
            }
        }

        /// <summary>
        ///A test for getCreatureY
        ///</summary>
        [TestMethod]
        public void GetCreatureYTest()
        {
            var sampleData = new[]
            {
                new { Map = "@", Y = 0},
                new { Map = ".@.", Y = 0},
                new { Map = "." + Environment.NewLine
                          + "@" + Environment.NewLine
                          + ".", Y = 1},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", Y = 1},
            };
            foreach (var item in sampleData)
            {
                Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(item.Map);
                int expected = item.Y;

                int actual = testDungeon.GetCreatureY(GameData.Ui.PlayerId);

                Assert.AreEqual(expected, actual, "Map: " + item.Map);
            }
        }

        /// <summary>
        ///A test for getCreature using the Location parameter
        ///</summary>
        [TestMethod]
        public void GetCreatureLocationParamTest()
        {
            var sampleData = new[]
            {
                new { Map = "@", Location = new Location(0, 0), Creature = CreatureFactory.GetNewPlayer()},
                new { Map = ".@.", Location = new Location(1, 0), Creature = CreatureFactory.GetNewPlayer()},
                new { Map = "." + Environment.NewLine
                          + "@" + Environment.NewLine
                          + ".", Location = new Location(0, 1), Creature = CreatureFactory.GetNewPlayer()},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", Location = new Location(1, 1), Creature = CreatureFactory.GetNewPlayer()},
                new { Map = "@", Location = new Location(-1, 0), Creature = (Creature) null},
                new { Map = "@", Location = new Location(0, -1), Creature = (Creature) null},
                new { Map = "@", Location = new Location(-1, -1), Creature = (Creature) null},
                new { Map = "@", Location = new Location(1, 0), Creature = (Creature) null},
                new { Map = "@", Location = new Location(0, 1), Creature = (Creature) null},
                new { Map = "@", Location = new Location(1, 1), Creature = (Creature) null},
            };
            foreach (var item in sampleData)
            {
                Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(item.Map);
                Creature expected = item.Creature;

                Creature actual = testDungeon.GetCreature(item.Location);

                Assert.AreEqual(true, expected == actual, "Location<" + item.Location + ">. Map: " + item.Map);
            }
        }

        /// <summary>
        ///A test for getCreature using the X and Y parameters
        ///</summary>
        [TestMethod]
        public void GetCreatureXYParamTest()
        {
            var sampleData = new[]
            {
                new { Map = "@", Location = new Location(0, 0), Creature = CreatureFactory.GetNewPlayer()},
                new { Map = ".@.", Location = new Location(1, 0), Creature = CreatureFactory.GetNewPlayer()},
                new { Map = "." + Environment.NewLine
                          + "@" + Environment.NewLine
                          + ".", Location = new Location(0, 1), Creature = CreatureFactory.GetNewPlayer()},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", Location = new Location(1, 1), Creature = CreatureFactory.GetNewPlayer()},
                new { Map = "@", Location = new Location(-1, 0), Creature = (Creature) null},
                new { Map = "@", Location = new Location(0, -1), Creature = (Creature) null},
                new { Map = "@", Location = new Location(-1, -1), Creature = (Creature) null},
                new { Map = "@", Location = new Location(1, 0), Creature = (Creature) null},
                new { Map = "@", Location = new Location(0, 1), Creature = (Creature) null},
                new { Map = "@", Location = new Location(1, 1), Creature = (Creature) null},
            };
            foreach (var item in sampleData)
            {
                Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(item.Map);
                Creature expected = item.Creature;

                Creature actual = testDungeon.GetCreature(item.Location.X, item.Location.Y);

                Assert.AreEqual(true, expected == actual, "X<" + item.Location.X + ">. Y<" + item.Location.Y + ">. Map: " + item.Map);
            }
        }

        /// <summary>
        ///A test for getCreature using the CreatureId parameter
        ///</summary>
        [TestMethod]
        public void GetCreatureIdParamTest()
        {
            var sampleData = new[]
            {
                new { Map = "@", Id = "PLAYER", Creature = CreatureFactory.GetNewPlayer()},
                new { Map = ".@.", Id = "PLAYER", Creature = CreatureFactory.GetNewPlayer()},
                new { Map = "." + Environment.NewLine
                          + "@" + Environment.NewLine
                          + ".", Id = "PLAYER", Creature = CreatureFactory.GetNewPlayer()},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", Id = "PLAYER", Creature = CreatureFactory.GetNewPlayer()},
                new { Map = "@", Id = "FUNGUS", Creature = (Creature) null},
            };
            foreach (var item in sampleData)
            {
                Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(item.Map);
                Creature expected = item.Creature;

                Creature actual = testDungeon.GetCreature(item.Id);

                Assert.AreEqual(true, expected == actual, "Id<" + item.Id + ">. Map: " + item.Map);
            }
        }

        /// <summary>
        ///A test for getDisplayValue
        ///</summary>
        [TestMethod]
        public void GetDisplayValueByLocationTest()
        {
            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap("#.=@+"+Environment.NewLine
                                                                     + "U_.=*");
            testDungeon.FOVState.Init2D(Dungeon.FOVStateType.Visible);

            var sampleData = new[]
            {
                new { Location = new Location(0, 0), ExpectedTile = GameData.Tiles["STONE_WALL"].Appearance },
                new { Location = new Location(1, 0), ExpectedTile = GameData.Tiles["STONE_FLOOR"].Appearance },
                new { Location = new Location(2, 0), ExpectedTile = GameData.Liquids["DEEP_WATER"].Appearance },
                new { Location = new Location(3, 0), ExpectedTile = testDungeon.CreatureList[GameData.Ui.PlayerId].Appearance },
                new { Location = new Location(4, 0), ExpectedTile = GameData.Fixtures["CLOSED_DOOR"].Appearance },
                new { Location = new Location(0, 1), ExpectedTile = GameData.Fixtures["FOUNTAIN"].Appearance },
                new { Location = new Location(1, 1), ExpectedTile = GameData.Fixtures["ALTAR"].Appearance },
                new { Location = new Location(2, 1), ExpectedTile = GameData.Tiles["STONE_FLOOR"].Appearance },
                new { Location = new Location(3, 1), ExpectedTile = GameData.Liquids["DEEP_WATER"].Appearance },
                new { Location = new Location(4, 1), ExpectedTile = GameData.Liquids["DEEP_WATER_WAVE_CREST"].Appearance },
            };
            foreach (var item in sampleData)
            {
                DisplayCharacter expected = item.ExpectedTile;

                DisplayCharacter actual = testDungeon.GetDisplayValue(item.Location);

                Assert.AreEqual(expected, actual, "Location<" + item.Location +">.");
            }
            Assert.Inconclusive("Need to add different FOVStates to the test.");
        }

        /// <summary>
        ///A test for getDisplayValue
        ///</summary>
        [TestMethod]
        public void GetDisplayValueByXYTest()
        {
            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap("#.=@+" + Environment.NewLine
                                                                     + "U_.=*");
            testDungeon.FOVState.Init2D(Dungeon.FOVStateType.Visible);

            var sampleData = new[]
            {
                new { Location = new Location(0, 0), ExpectedTile = GameData.Tiles["STONE_WALL"].Appearance },
                new { Location = new Location(1, 0), ExpectedTile = GameData.Tiles["STONE_FLOOR"].Appearance },
                new { Location = new Location(2, 0), ExpectedTile = GameData.Liquids["DEEP_WATER"].Appearance },
                new { Location = new Location(3, 0), ExpectedTile = testDungeon.CreatureList[GameData.Ui.PlayerId].Appearance },
                new { Location = new Location(4, 0), ExpectedTile = GameData.Fixtures["CLOSED_DOOR"].Appearance },
                new { Location = new Location(0, 1), ExpectedTile = GameData.Fixtures["FOUNTAIN"].Appearance },
                new { Location = new Location(1, 1), ExpectedTile = GameData.Fixtures["ALTAR"].Appearance },
                new { Location = new Location(2, 1), ExpectedTile = GameData.Tiles["STONE_FLOOR"].Appearance },
                new { Location = new Location(3, 1), ExpectedTile = GameData.Liquids["DEEP_WATER"].Appearance },
                new { Location = new Location(4, 1), ExpectedTile = GameData.Liquids["DEEP_WATER_WAVE_CREST"].Appearance },
            };
            foreach (var item in sampleData)
            {
                DisplayCharacter expected = item.ExpectedTile;

                DisplayCharacter actual = testDungeon.GetDisplayValue(item.Location.X, item.Location.Y);

                Assert.AreEqual(expected, actual, "X<" + item.Location.X + ">. Y<" + item.Location.Y + ">.");
            }
            Assert.Inconclusive("Need to add different FOVStates to the test.");
        }

        /// <summary>
        ///A test for getFixture
        ///</summary>
        [TestMethod]
        public void GetFixtureTest()
        {
            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap("#.=@+" + Environment.NewLine
                                                                     + "U_.=*");

            var sampleData = new[]
            {
                new { Location = new Location(0, 0), ExpectedTile = new Fixture() },
                new { Location = new Location(1, 0), ExpectedTile = new Fixture() },
                new { Location = new Location(2, 0), ExpectedTile = new Fixture() },
                new { Location = new Location(3, 0), ExpectedTile = new Fixture() },
                new { Location = new Location(4, 0), ExpectedTile = GameData.Fixtures["CLOSED_DOOR"] },
                new { Location = new Location(0, 1), ExpectedTile = GameData.Fixtures["FOUNTAIN"] },
                new { Location = new Location(1, 1), ExpectedTile = GameData.Fixtures["ALTAR"] },
                new { Location = new Location(2, 1), ExpectedTile = new Fixture() },
                new { Location = new Location(3, 1), ExpectedTile = new Fixture() },
                new { Location = new Location(4, 1), ExpectedTile = new Fixture() },
                new { Location = new Location(-1, 0), ExpectedTile = new Fixture() },
                new { Location = new Location(0, -1), ExpectedTile = new Fixture() },
                new { Location = new Location(-1, -1), ExpectedTile = new Fixture() },
                new { Location = new Location(5, 1), ExpectedTile = new Fixture() },
                new { Location = new Location(4, 2), ExpectedTile = new Fixture() },
                new { Location = new Location(5, 2), ExpectedTile = new Fixture() },
            };

            foreach (var item in sampleData)
            {
                Fixture expected = item.ExpectedTile;

                Fixture actual = testDungeon.GetFixture(item.Location.X, item.Location.Y);

                Assert.AreEqual(expected.Type, actual.Type, "Fixture Expected<" + expected.ToString() + ">. Fixture Actual<" + actual.ToString() + ">. X<" + item.Location.X + ">. Y<" + item.Location.Y + ">.");
            }

            foreach (var item in sampleData)
            {
                Fixture expected = item.ExpectedTile;

                Fixture actual = testDungeon.GetFixture(item.Location);

                Assert.AreEqual(expected.Type, actual.Type, "Fixture Expected<" + expected.ToString() + ">. Fixture Actual<" + actual.ToString() + ">. Location<" + item.Location + ">.");
            }
        }

        /// <summary>
        ///A test for getLiquid
        ///</summary>
        [TestMethod]
        public void GetLiquidTest()
        {
            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap("#.=@+" + Environment.NewLine
                                                                     + "U_.=*");

            var sampleData = new[]
            {
                new { Location = new Location(0, 0), ExpectedTile = new Liquid() },
                new { Location = new Location(1, 0), ExpectedTile = new Liquid() },
                new { Location = new Location(2, 0), ExpectedTile = GameData.Liquids["DEEP_WATER"] },
                new { Location = new Location(3, 0), ExpectedTile = new Liquid() },
                new { Location = new Location(4, 0), ExpectedTile = new Liquid() },
                new { Location = new Location(0, 1), ExpectedTile = new Liquid() },
                new { Location = new Location(1, 1), ExpectedTile = new Liquid() },
                new { Location = new Location(2, 1), ExpectedTile = new Liquid() },
                new { Location = new Location(3, 1), ExpectedTile = GameData.Liquids["DEEP_WATER"] },
                new { Location = new Location(4, 1), ExpectedTile = GameData.Liquids["DEEP_WATER_WAVE_CREST"] },
                new { Location = new Location(-1, 0), ExpectedTile = new Liquid() },
                new { Location = new Location(0, -1), ExpectedTile = new Liquid() },
                new { Location = new Location(-1, -1), ExpectedTile = new Liquid() },
                new { Location = new Location(5, 1), ExpectedTile = new Liquid() },
                new { Location = new Location(4, 2), ExpectedTile = new Liquid() },
                new { Location = new Location(5, 2), ExpectedTile = new Liquid() },
            };

            foreach (var item in sampleData)
            {
                Liquid expected = item.ExpectedTile;

                Liquid actual = testDungeon.GetLiquid(item.Location.X, item.Location.Y);

                Assert.AreEqual(expected.Type, actual.Type, "Liquid Expected<" + expected.ToString() + ">. Liquid Actual<" + actual.ToString() + ">. X<" + item.Location.X + ">. Y<" + item.Location.Y + ">.");
            }

            foreach (var item in sampleData)
            {
                Liquid expected = item.ExpectedTile;

                Liquid actual = testDungeon.GetLiquid(item.Location);

                Assert.AreEqual(expected.Type, actual.Type, "Liquid Expected<" + expected.ToString() + ">. Liquid Actual<" + actual.ToString() + ">. Location<" + item.Location + ">.");
            }
        }

        /// <summary>
        ///A test for GetPassableDirections
        ///</summary>
        [TestMethod()]
        public void GetPassableDirectionsTest()
        {
            string map = @"######" + Environment.NewLine
                       + @"#....#" + Environment.NewLine
                       + @"#....#" + Environment.NewLine
                       + @"######";

            Tuple<Location, List<Direction>>[] sampleData = { new Tuple<Location, List<Direction>>(new Location(0, 0), new List<Direction>{Direction.SouthEast}),
                                                              new Tuple<Location, List<Direction>>(new Location(3, 2), new List<Direction>{Direction.North,
                                                                                                                                           Direction.NorthEast,
                                                                                                                                           Direction.East,
                                                                                                                                           Direction.West,
                                                                                                                                           Direction.NorthWest}),
                                                              };

            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(map);

            foreach (Tuple<Location, List<Direction>> currentData in sampleData)
            {
                List<Direction> expected = currentData.Item2;
                expected.Sort();
                List<Direction> actual = testDungeon.GetPassableDirections(currentData.Item1, GameData.Ui.PlayerId);
                actual.Sort();
                CollectionAssert.AreEqual(expected, actual, "Location<" + currentData.Item1 + ">.");
            }
        }

        /// <summary>
        ///A test for getTile
        ///</summary>
        [TestMethod]
        public void GetTileTest()
        {
            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap("#.=@+" + Environment.NewLine
                                                                     + "U_.=*");

            var sampleData = new[]
            {
                new { Location = new Location(0, 0), ExpectedTile = GameData.Tiles["STONE_WALL"] },
                new { Location = new Location(1, 0), ExpectedTile = GameData.Tiles["STONE_FLOOR"] },
                new { Location = new Location(2, 0), ExpectedTile = GameData.Tiles["STONE_CAVITY"] },
                new { Location = new Location(3, 0), ExpectedTile = GameData.Tiles["STONE_FLOOR"] },
                new { Location = new Location(4, 0), ExpectedTile = GameData.Tiles["STONE_FLOOR"] },
                new { Location = new Location(0, 1), ExpectedTile = GameData.Tiles["STONE_FLOOR"] },
                new { Location = new Location(1, 1), ExpectedTile = GameData.Tiles["STONE_FLOOR"] },
                new { Location = new Location(2, 1), ExpectedTile = GameData.Tiles["STONE_FLOOR"] },
                new { Location = new Location(3, 1), ExpectedTile = GameData.Tiles["STONE_CAVITY"] },
                new { Location = new Location(4, 1), ExpectedTile = GameData.Tiles["STONE_CAVITY"] },
                new { Location = new Location(-1, 0), ExpectedTile = new Tile() },
                new { Location = new Location(0, -1), ExpectedTile = new Tile() },
                new { Location = new Location(-1, -1), ExpectedTile = new Tile() },
                new { Location = new Location(5, 1), ExpectedTile = new Tile() },
                new { Location = new Location(4, 2), ExpectedTile = new Tile() },
                new { Location = new Location(5, 2), ExpectedTile = new Tile() },
            };

            foreach (var item in sampleData)
            {
                Tile expected = item.ExpectedTile;

                Tile actual = testDungeon.GetTile(item.Location.X, item.Location.Y);

                Assert.AreEqual(expected.Type, actual.Type, "Tile Expected<" + expected.ToString() + ">. Tile Actual<" + actual.ToString() + ">. X<" + item.Location.X + ">. Y<" + item.Location.Y + ">.");
            }

            foreach (var item in sampleData)
            {
                Tile expected = item.ExpectedTile;

                Tile actual = testDungeon.GetTile(item.Location);

                Assert.AreEqual(expected.Type, actual.Type, "Tile Expected<" + expected.ToString() + ">. Tile Actual<" + actual.ToString() + ">. Location<" + item.Location + ">.");
            }
        }

        /// <summary>
        ///A test for getTriggerDirections
        ///</summary>
        [TestMethod()]
        public void GetTriggerDirectionsTest()
        {
            string map = @"######" + Environment.NewLine
                       + @"#+,..#" + Environment.NewLine
                       + @"#.../#" + Environment.NewLine
                       + @"######";

            Tuple<Location, string, List<Direction>>[] sampleData = { new Tuple<Location, string, List<Direction>>(new Location(0, 0), "OPEN", new List<Direction>{Direction.SouthEast}),
                                                                      new Tuple<Location, string, List<Direction>>(new Location(2, 1), "OPEN", new List<Direction>{Direction.West}),
                                                                      new Tuple<Location, string, List<Direction>>(new Location(3, 1), "OPEN", new List<Direction>{}),
                                                                      new Tuple<Location, string, List<Direction>>(new Location(5, 1), "CLOSE", new List<Direction>{Direction.SouthWest}),
                                                                      new Tuple<Location, string, List<Direction>>(new Location(3, 3), "CLOSE", new List<Direction>{Direction.NorthEast}),
                                                                      new Tuple<Location, string, List<Direction>>(new Location(0, 0), "CLOSE", new List<Direction>{}),
                                                                      new Tuple<Location, string, List<Direction>>(new Location(2, 2), "HANDLE", new List<Direction>{Direction.North}),
                                                                      new Tuple<Location, string, List<Direction>>(new Location(2, 1), "HANDLE", new List<Direction>{Direction.Here}),
                                                                      new Tuple<Location, string, List<Direction>>(new Location(0, 0), "HANDLE", new List<Direction>{}),
                                                                      };

            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(map);

            foreach (Tuple<Location, string, List<Direction>> currentData in sampleData)
            {
                List<Direction> expected = currentData.Item3;
                expected.Sort();
                List<Direction> actual = testDungeon.GetTriggerDirections(currentData.Item1, currentData.Item2);
                actual.Sort();
                CollectionAssert.AreEqual(expected, actual, "Location<" + currentData.Item1 + ">. Trigger<" + currentData.Item2 + ">.");
            }
        }

        /// <summary>
        ///A test for hasDirectionTrigger
        ///</summary>
        [TestMethod()]
        public void HasDirectionTriggerTest()
        {
            string map = @"######" + Environment.NewLine
                       + @"#+,..#" + Environment.NewLine
                       + @"#.../#" + Environment.NewLine
                       + @"######";

            Tuple<Location, Direction, string, bool>[] sampleData = { new Tuple<Location, Direction, string, bool>(new Location(0, 0), Direction.North, "OPEN", false),
                                                                      new Tuple<Location, Direction, string, bool>(new Location(0, 0), Direction.SouthEast, "OPEN", true),
                                                                      new Tuple<Location, Direction, string, bool>(new Location(1, 2), Direction.North, "OPEN", true),
                                                                      new Tuple<Location, Direction, string, bool>(new Location(5, 3), Direction.NorthWest, "CLOSE", true),
                                                                      new Tuple<Location, Direction, string, bool>(new Location(0, 1), Direction.East, "CLOSE", false),
                                                                      new Tuple<Location, Direction, string, bool>(new Location(4, 1), Direction.South, "CLOSE", true),
                                                                      new Tuple<Location, Direction, string, bool>(new Location(3, 1), Direction.West, "HANDLE", true),
                                                                      new Tuple<Location, Direction, string, bool>(new Location(3, 1), Direction.SouthWest, "HANDLE", false),
                                                                      };

            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(map);

            foreach (Tuple<Location, Direction, string, bool> currentData in sampleData)
            {
                bool expected = currentData.Item4;
                bool actual = testDungeon.HasDirectionTrigger(currentData.Item1, currentData.Item2, currentData.Item3);
                Assert.AreEqual(expected, actual, "Location<" + currentData.Item1 + ">. Direction<" + currentData.Item2 + ">. Trigger<" + currentData.Item3 + ">.");
            }
        }

        /// <summary>
        ///A test for hasTrigger
        ///</summary>
        [TestMethod()]
        public void HasTriggerTest()
        {
            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap("#.=+/,");

            Tuple<Location, string, bool>[] sampleData = { new Tuple<Location, string, bool>(new Location(0, 0), "OPEN", false),
                                                           new Tuple<Location, string, bool>(new Location(1, 0), "OPEN", false),
                                                           new Tuple<Location, string, bool>(new Location(2, 0), "OPEN", false),
                                                           new Tuple<Location, string, bool>(new Location(3, 0), "OPEN", true),
                                                           new Tuple<Location, string, bool>(new Location(4, 0), "OPEN", false),
                                                           new Tuple<Location, string, bool>(new Location(5, 0), "OPEN", false),
                                                           new Tuple<Location, string, bool>(new Location(-1, 0), "OPEN", false),
                                                           new Tuple<Location, string, bool>(new Location(0, -1), "OPEN", false),
                                                           new Tuple<Location, string, bool>(new Location(10, 0), "OPEN", false),
                                                           new Tuple<Location, string, bool>(new Location(0, 10), "OPEN", false),
                                                           new Tuple<Location, string, bool>(new Location(0, 0), "CLOSE", false),
                                                           new Tuple<Location, string, bool>(new Location(1, 0), "CLOSE", false),
                                                           new Tuple<Location, string, bool>(new Location(2, 0), "CLOSE", false),
                                                           new Tuple<Location, string, bool>(new Location(3, 0), "CLOSE", false),
                                                           new Tuple<Location, string, bool>(new Location(4, 0), "CLOSE", true),
                                                           new Tuple<Location, string, bool>(new Location(5, 0), "CLOSE", false),
                                                           new Tuple<Location, string, bool>(new Location(-1, 0), "CLOSE", false),
                                                           new Tuple<Location, string, bool>(new Location(0, -1), "CLOSE", false),
                                                           new Tuple<Location, string, bool>(new Location(10, 0), "CLOSE", false),
                                                           new Tuple<Location, string, bool>(new Location(0, 10), "CLOSE", false),
                                                           new Tuple<Location, string, bool>(new Location(0, 0), "HANDLE", false),
                                                           new Tuple<Location, string, bool>(new Location(1, 0), "HANDLE", false),
                                                           new Tuple<Location, string, bool>(new Location(2, 0), "HANDLE", false),
                                                           new Tuple<Location, string, bool>(new Location(3, 0), "HANDLE", false),
                                                           new Tuple<Location, string, bool>(new Location(4, 0), "HANDLE", false),
                                                           new Tuple<Location, string, bool>(new Location(5, 0), "HANDLE", true),
                                                           new Tuple<Location, string, bool>(new Location(-1, 0), "HANDLE", false),
                                                           new Tuple<Location, string, bool>(new Location(0, -1), "HANDLE", false),
                                                           new Tuple<Location, string, bool>(new Location(10, 0), "HANDLE", false),
                                                           new Tuple<Location, string, bool>(new Location(0, 10), "HANDLE", false),
                                                           };

            foreach (Tuple<Location, string, bool> currentData in sampleData)
            {
                bool expected = currentData.Item3;
                bool actual = testDungeon.HasTrigger(currentData.Item1, currentData.Item2);
                Assert.AreEqual(expected, actual, "Location<" + currentData.Item1 + ">. Trigger<" + currentData.Item2 + ">.");
            }
        }

        /// <summary>
        ///A test for Height
        ///</summary>
        [TestMethod]
        public void HeightTest()
        {
            var sampleData = new[]
            {
                new { Map = "@", Height = 1},
                new { Map = ".@.", Height = 1},
                new { Map = "." + Environment.NewLine
                          + "@" + Environment.NewLine
                          + ".", Height = 3},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", Height = 3},
            };
            foreach (var item in sampleData)
            {
                Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(item.Map);
                int expected = item.Height;

                int actual = testDungeon.Height;

                Assert.AreEqual(expected, actual, "Map: " + item.Map);
            }
        }

        /// <summary>
        ///A test for IsDirectionPassable
        ///</summary>
        [TestMethod()]
        public void IsDirectionPassableTest()
        {
            string map = @"######" + Environment.NewLine
                       + @"#....#" + Environment.NewLine
                       + @"#....#" + Environment.NewLine
                       + @"######";

            Tuple<Location, Direction, bool>[] sampleData = { new Tuple<Location, Direction, bool>(new Location(0, 0), Direction.North, false),
                                                              new Tuple<Location, Direction, bool>(new Location(0, 0), Direction.SouthEast, true),
                                                              new Tuple<Location, Direction, bool>(new Location(3, 2), Direction.North, true),
                                                              };

            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(map);

            foreach (Tuple<Location, Direction, bool> currentData in sampleData)
            {
                bool expected = currentData.Item3;
                bool actual = testDungeon.IsDirectionPassable(currentData.Item1, currentData.Item2, GameData.Ui.PlayerId);
                Assert.AreEqual(expected, actual, "Location<" + currentData.Item1 + ">. Direction<" + currentData.Item2 + ">.");
            }
        }

        /// <summary>
        ///A test for IsPassable using the Location parameter
        ///</summary>
        [TestMethod]
        public void IsPassableLocationTest()
        {
            Tuple<Location, bool>[] sampleData = { new Tuple<Location, bool>(new Location(0, 0), false),
                                                   new Tuple<Location, bool>(new Location(1, 0), true),
                                                   new Tuple<Location, bool>(new Location(2, 0), false),
                                                   new Tuple<Location, bool>(new Location(-1, 0), false),
                                                   new Tuple<Location, bool>(new Location(0, -1), false),
                                                   new Tuple<Location, bool>(new Location(10, 0), false),
                                                   new Tuple<Location, bool>(new Location(0, 10), false),
                                                   };

            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap("#.=");

            foreach (Tuple<Location, bool> currentData in sampleData)
            {
                bool expected = currentData.Item2;
                bool actual = testDungeon.IsPassable(currentData.Item1, GameData.Ui.PlayerId);
                Assert.AreEqual(expected, actual, "Location<" + currentData.Item1 + ">.");
            }
        }

        /// <summary>
        ///A test for IsPassable using the X and Y parameters
        ///</summary>
        [TestMethod]
        public void IsPassableXyTest()
        {
            Tuple<Location, bool>[] sampleData = { new Tuple<Location, bool>(new Location(0, 0), false),
                                                   new Tuple<Location, bool>(new Location(1, 0), true),
                                                   new Tuple<Location, bool>(new Location(2, 0), false),
                                                   new Tuple<Location, bool>(new Location(-1, 0), false),
                                                   new Tuple<Location, bool>(new Location(0, -1), false),
                                                   new Tuple<Location, bool>(new Location(10, 0), false),
                                                   new Tuple<Location, bool>(new Location(0, 10), false),
                                                   };

            Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap("#.=");

            foreach (Tuple<Location, bool> currentData in sampleData)
            {
                bool expected = currentData.Item2;
                int x = currentData.Item1.X;
                int y = currentData.Item1.Y;
                bool actual = testDungeon.IsPassable(x, y, GameData.Ui.PlayerId);
                Assert.AreEqual(expected, actual, "Location<" + currentData.Item1 + ">.");
            }
        }

        /// <summary>
        ///A test for Liquids
        ///</summary>
        [TestMethod]
        public void LiquidsTest()
        {
            Dungeon target = new Dungeon(); // TODO: Initialize to an appropriate value
            string[,] expected = null; // TODO: Initialize to an appropriate value
            target.Liquids = expected;
            string[,] actual = target.Liquids;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for moveCreature
        ///</summary>
        [TestMethod]
        public void MoveCreatureTest()
        {
            var sampleData = new[]
            {
                new { Map = "@", MoveDirection = Direction.North, FinalLocation = new Location(0, 0)},
                new { Map = "@", MoveDirection = Direction.South, FinalLocation = new Location(0, 0)},
                new { Map = "@", MoveDirection = Direction.East, FinalLocation = new Location(0, 0)},
                new { Map = "@", MoveDirection = Direction.West, FinalLocation = new Location(0, 0)},
                new { Map = "@", MoveDirection = Direction.NorthEast, FinalLocation = new Location(0, 0)},
                new { Map = "@", MoveDirection = Direction.NorthWest, FinalLocation = new Location(0, 0)},
                new { Map = "@", MoveDirection = Direction.SouthEast, FinalLocation = new Location(0, 0)},
                new { Map = "@", MoveDirection = Direction.SouthWest, FinalLocation = new Location(0, 0)},
                //new { Map = "@", MoveDirection = Direction.Up, FinalLocation = new Location(0, 0)},
                //new { Map = "@", MoveDirection = Direction.Down, FinalLocation = new Location(0, 0)},
                //new { Map = "@", MoveDirection = Direction.Here, FinalLocation = new Location(0, 0)},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", MoveDirection = Direction.North, FinalLocation = new Location(1, 0)},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", MoveDirection = Direction.South, FinalLocation = new Location(1, 2)},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", MoveDirection = Direction.East, FinalLocation = new Location(2, 1)},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", MoveDirection = Direction.West, FinalLocation = new Location(0, 1)},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", MoveDirection = Direction.NorthEast, FinalLocation = new Location(2, 0)},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", MoveDirection = Direction.NorthWest, FinalLocation = new Location(0, 0)},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", MoveDirection = Direction.SouthEast, FinalLocation = new Location(2, 2)},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", MoveDirection = Direction.SouthWest, FinalLocation = new Location(0, 2)},
                //new { Map = "..." + Environment.NewLine
                          //+ ".@." + Environment.NewLine
                          //+ "...", MoveDirection = Direction.Up, FinalLocation = new Location(0, 0)},
                //new { Map = "..." + Environment.NewLine
                          //+ ".@." + Environment.NewLine
                          //+ "...", MoveDirection = Direction.Down, FinalLocation = new Location(0, 0)},
                //new { Map = "..." + Environment.NewLine
                          //+ ".@." + Environment.NewLine
                          //+ "...", MoveDirection = Direction.Here, FinalLocation = new Location(0, 0)},
                new { Map = "###" + Environment.NewLine
                          + "#@#" + Environment.NewLine
                          + "###", MoveDirection = Direction.North, FinalLocation = new Location(1, 1)},
                new { Map = "###" + Environment.NewLine
                          + "#@#" + Environment.NewLine
                          + "###", MoveDirection = Direction.South, FinalLocation = new Location(1, 1)},
                new { Map = "###" + Environment.NewLine
                          + "#@#" + Environment.NewLine
                          + "###", MoveDirection = Direction.East, FinalLocation = new Location(1, 1)},
                new { Map = "###" + Environment.NewLine
                          + "#@#" + Environment.NewLine
                          + "###", MoveDirection = Direction.West, FinalLocation = new Location(1, 1)},
                new { Map = "###" + Environment.NewLine
                          + "#@#" + Environment.NewLine
                          + "###", MoveDirection = Direction.NorthEast, FinalLocation = new Location(1, 1)},
                new { Map = "###" + Environment.NewLine
                          + "#@#" + Environment.NewLine
                          + "###", MoveDirection = Direction.NorthWest, FinalLocation = new Location(1, 1)},
                new { Map = "###" + Environment.NewLine
                          + "#@#" + Environment.NewLine
                          + "###", MoveDirection = Direction.SouthEast, FinalLocation = new Location(1, 1)},
                new { Map = "###" + Environment.NewLine
                          + "#@#" + Environment.NewLine
                          + "###", MoveDirection = Direction.SouthWest, FinalLocation = new Location(1, 1)},
                //new { Map = "###" + Environment.NewLine
                          //+ "#@#" + Environment.NewLine
                          //+ "###", MoveDirection = Direction.Up, FinalLocation = new Location(0, 0)},
                //new { Map = "###" + Environment.NewLine
                          //+ "#@#" + Environment.NewLine
                          //+ "###", MoveDirection = Direction.Down, FinalLocation = new Location(0, 0)},
                //new { Map = "###" + Environment.NewLine
                          //+ "#@#" + Environment.NewLine
                          //+ "###", MoveDirection = Direction.Here, FinalLocation = new Location(0, 0)},
            };
            foreach (var item in sampleData)
            {
                Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(item.Map);
                Location expected = item.FinalLocation;

                testDungeon.MoveCreature(GameData.Ui.PlayerId, item.MoveDirection);

                Location actual = testDungeon.GetCreatureLocation(GameData.Ui.PlayerId);

                Assert.AreEqual(expected, actual, "Map: " + item.Map);
            }
        }

        /// <summary>
        ///A test for Name
        ///</summary>
        [TestMethod]
        public void NameTest()
        {
            var sampleData = new[]
            {
                new { Name = "Test"},
                new { Name = "Hi!"},
                new { Name = "123"},
                new { Name = "Fun times were had by all"},
                new { Name = "This is the test that never ends, it just goes on and on my friends."},
            };
            foreach (var item in sampleData)
            {
                Dungeon testDungeon = new Dungeon();
                testDungeon.Name = item.Name;

                string expected = item.Name;

                string actual = testDungeon.Name;

                Assert.AreEqual(expected, actual, "Name<" + item.Name + ">");
            }
        }

        /// <summary>
        ///A test for Tiles
        ///</summary>
        [TestMethod]
        public void TilesTest()
        {
            Dungeon target = new Dungeon(); // TODO: Initialize to an appropriate value
            string[,] expected = null; // TODO: Initialize to an appropriate value
            target.Tiles = expected;
            string[,] actual = target.Tiles;
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
        
        /// <summary>
        ///A test for triggerAdjacent
        ///</summary>
        [TestMethod]
        public void TriggerAdjacentTest()
        {
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
        /// <summary>
        ///A test for triggerCell
        ///</summary>
        [TestMethod]
        public void TriggerCellTest()
        {
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        //    #region sampleData
        /// <summary>
        ///A test for updateCells
        ///</summary>
        [TestMethod]
        public void UpdateCellsTest()
        {
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
        
        ///// <summary>
        /////A test for GetPathwayType
        /////</summary>
        //[TestMethod()]
        //public void GetPathwayTypeTest()
        //{
        //    string map = @"###########" + Environment.NewLine
        //               + @"#...#.....#" + Environment.NewLine
        //               + @"#...#.###.#" + Environment.NewLine
        //               + @"#...#...#.#" + Environment.NewLine
        //               + @"#######.#.#" + Environment.NewLine
        //               + @"#...#...###" + Environment.NewLine
        //               + @"#.......#.#" + Environment.NewLine
        //               + @"#...#....##" + Environment.NewLine
        //               + @"###########";
        //
        //    Tuple<Location, PathwayType>[] sampleData = { new Tuple<Location, PathwayType>(new Location(0, 0), PathwayType.Impassable),
        //                                                  new Tuple<Location, PathwayType>(new Location(1, 1), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(1, 2), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(1, 3), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(1, 5), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(1, 6), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(1, 7), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(2, 1), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(2, 2), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(2, 3), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(2, 5), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(2, 6), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(2, 7), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(3, 1), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(3, 2), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(3, 3), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(3, 5), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(3, 6), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(3, 7), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(4, 6), PathwayType.Doorway),
        //                                                  new Tuple<Location, PathwayType>(new Location(5, 1), PathwayType.CorridorCorner),
        //                                                  new Tuple<Location, PathwayType>(new Location(5, 2), PathwayType.CorridorStraight),
        //                                                  new Tuple<Location, PathwayType>(new Location(5, 3), PathwayType.CorridorCorner),
        //                                                  new Tuple<Location, PathwayType>(new Location(5, 5), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(5, 6), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(5, 7), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(6, 1), PathwayType.CorridorStraight),
        //                                                  new Tuple<Location, PathwayType>(new Location(6, 3), PathwayType.CorridorStraight),
        //                                                  new Tuple<Location, PathwayType>(new Location(6, 5), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(6, 6), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(6, 7), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(7, 1), PathwayType.CorridorStraight),
        //                                                  new Tuple<Location, PathwayType>(new Location(7, 3), PathwayType.CorridorCorner),
        //                                                  new Tuple<Location, PathwayType>(new Location(7, 4), PathwayType.Doorway),
        //                                                  new Tuple<Location, PathwayType>(new Location(7, 5), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(7, 6), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(7, 7), PathwayType.Room),
        //                                                  new Tuple<Location, PathwayType>(new Location(8, 1), PathwayType.CorridorStraight),
        //                                                  new Tuple<Location, PathwayType>(new Location(8, 7), PathwayType.Doorway),
        //                                                  new Tuple<Location, PathwayType>(new Location(9, 1), PathwayType.CorridorCorner),
        //                                                  new Tuple<Location, PathwayType>(new Location(9, 2), PathwayType.CorridorStraight),
        //                                                  new Tuple<Location, PathwayType>(new Location(9, 3), PathwayType.CorridorStraight),
        //                                                  new Tuple<Location, PathwayType>(new Location(9, 4), PathwayType.DeadEnd),
        //                                                  new Tuple<Location, PathwayType>(new Location(9, 6), PathwayType.DeadEnd),
        //                                                  };
        //
        //    foreach (Tuple<Location, PathwayType> currentData in sampleData)
        //    {
        //        PathwayType expected = currentData.Item2;
        //        PathwayType actual = testDungeon.GetPathwayType(currentData.Item1, GameData.Ui.PlayerId);
        //        Assert.AreEqual(expected, actual, "Location<" + currentData.Item1 + ">");
        //    }
        //}

        /// <summary>
        ///A test for Width
        ///</summary>
        [TestMethod]
        public void WidthTest()
        {
            var sampleData = new[]
            {
                new { Map = "@", Width = 1},
                new { Map = ".@.", Width = 3},
                new { Map = "." + Environment.NewLine
                          + "@" + Environment.NewLine
                          + ".", Width = 1},
                new { Map = "..." + Environment.NewLine
                          + ".@." + Environment.NewLine
                          + "...", Width = 3},
            };
            foreach (var item in sampleData)
            {
                Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(item.Map);
                int expected = item.Width;

                int actual = testDungeon.Width;

                Assert.AreEqual(expected, actual, "Map: " + item.Map);
            }
        }

        [TestMethod()]
        public void IsPossibleDoorwayTest()
        {
            var sampleData = new[]
            {
                new { Iteration = -02, Map = ".#" + Environment.NewLine
                                           + ".." + Environment.NewLine
                                           + ".#", Expected = false},
                new { Iteration = -01, Map = "..." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 000, Map = "..." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 001, Map = "..." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 002, Map = "..." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 003, Map = "..." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 004, Map = "..." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 005, Map = "..." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 006, Map = "..." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 007, Map = "..." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 008, Map = "..." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 009, Map = "..." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 010, Map = "..." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 011, Map = "..." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 012, Map = "..." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 013, Map = "..." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 014, Map = "..." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 015, Map = "..." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 016, Map = "..." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 017, Map = "..." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 018, Map = "..." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 019, Map = "..." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 020, Map = "..." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 021, Map = "..." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 022, Map = "..." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 023, Map = "..." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 024, Map = "..." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "...", Expected = true},
                new { Iteration = 025, Map = "..." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "..#", Expected = true},
                new { Iteration = 026, Map = "..." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 027, Map = "..." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".##", Expected = true},
                new { Iteration = 028, Map = "..." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#..", Expected = true},
                new { Iteration = 029, Map = "..." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#.#", Expected = true},
                new { Iteration = 030, Map = "..." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "##.", Expected = true},
                new { Iteration = 031, Map = "..." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 032, Map = "..#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 033, Map = "..#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 034, Map = "..#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 035, Map = "..#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 036, Map = "..#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 037, Map = "..#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 038, Map = "..#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 039, Map = "..#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 040, Map = "..#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 041, Map = "..#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 042, Map = "..#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 043, Map = "..#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 044, Map = "..#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 045, Map = "..#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 046, Map = "..#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 047, Map = "..#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 048, Map = "..#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 049, Map = "..#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 050, Map = "..#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 051, Map = "..#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 052, Map = "..#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 053, Map = "..#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 054, Map = "..#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 055, Map = "..#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 056, Map = "..#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "...", Expected = true},
                new { Iteration = 057, Map = "..#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "..#", Expected = true},
                new { Iteration = 058, Map = "..#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 059, Map = "..#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 060, Map = "..#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#..", Expected = true},
                new { Iteration = 061, Map = "..#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#.#", Expected = true},
                new { Iteration = 062, Map = "..#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "##.", Expected = true},
                new { Iteration = 063, Map = "..#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 064, Map = ".#." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 065, Map = ".#." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 066, Map = ".#." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 067, Map = ".#." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".##", Expected = true},
                new { Iteration = 068, Map = ".#." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 069, Map = ".#." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 070, Map = ".#." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "##.", Expected = true},
                new { Iteration = 071, Map = ".#." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "###", Expected = true},
                new { Iteration = 072, Map = ".#." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 073, Map = ".#." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 074, Map = ".#." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 075, Map = ".#." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".##", Expected = true},
                new { Iteration = 076, Map = ".#." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 077, Map = ".#." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 078, Map = ".#." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "##.", Expected = true},
                new { Iteration = 079, Map = ".#." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 080, Map = ".#." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 081, Map = ".#." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 082, Map = ".#." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 083, Map = ".#." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".##", Expected = true},
                new { Iteration = 084, Map = ".#." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 085, Map = ".#." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 086, Map = ".#." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "##.", Expected = true},
                new { Iteration = 087, Map = ".#." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 088, Map = ".#." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "...", Expected = true},
                new { Iteration = 089, Map = ".#." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "..#", Expected = true},
                new { Iteration = 090, Map = ".#." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 091, Map = ".#." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".##", Expected = true},
                new { Iteration = 092, Map = ".#." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#..", Expected = true},
                new { Iteration = 093, Map = ".#." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 094, Map = ".#." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "##.", Expected = true},
                new { Iteration = 095, Map = ".#." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 096, Map = ".##" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 097, Map = ".##" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 098, Map = ".##" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 099, Map = ".##" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".##", Expected = true},
                new { Iteration = 100, Map = ".##" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 101, Map = ".##" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 102, Map = ".##" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "##.", Expected = true},
                new { Iteration = 103, Map = ".##" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "###", Expected = true},
                new { Iteration = 104, Map = ".##" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 105, Map = ".##" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 106, Map = ".##" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 107, Map = ".##" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 108, Map = ".##" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 109, Map = ".##" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 110, Map = ".##" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "##.", Expected = true},
                new { Iteration = 111, Map = ".##" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 112, Map = ".##" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 113, Map = ".##" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 114, Map = ".##" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 115, Map = ".##" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 116, Map = ".##" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 117, Map = ".##" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 118, Map = ".##" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "##.", Expected = true},
                new { Iteration = 119, Map = ".##" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 120, Map = ".##" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "...", Expected = true},
                new { Iteration = 121, Map = ".##" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 122, Map = ".##" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 123, Map = ".##" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 124, Map = ".##" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#..", Expected = true},
                new { Iteration = 125, Map = ".##" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 126, Map = ".##" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 127, Map = ".##" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 128, Map = "#.." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 129, Map = "#.." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 130, Map = "#.." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 131, Map = "#.." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 132, Map = "#.." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 133, Map = "#.." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 134, Map = "#.." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 135, Map = "#.." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 136, Map = "#.." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 137, Map = "#.." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 138, Map = "#.." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 139, Map = "#.." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 140, Map = "#.." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 141, Map = "#.." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 142, Map = "#.." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 143, Map = "#.." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 144, Map = "#.." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 145, Map = "#.." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 146, Map = "#.." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 147, Map = "#.." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 148, Map = "#.." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 149, Map = "#.." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 150, Map = "#.." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 151, Map = "#.." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 152, Map = "#.." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "...", Expected = true},
                new { Iteration = 153, Map = "#.." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "..#", Expected = true},
                new { Iteration = 154, Map = "#.." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 155, Map = "#.." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".##", Expected = true},
                new { Iteration = 156, Map = "#.." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#..", Expected = true},
                new { Iteration = 157, Map = "#.." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#.#", Expected = true},
                new { Iteration = 158, Map = "#.." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 159, Map = "#.." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 160, Map = "#.#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 161, Map = "#.#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 162, Map = "#.#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 163, Map = "#.#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 164, Map = "#.#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 165, Map = "#.#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 166, Map = "#.#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 167, Map = "#.#" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 168, Map = "#.#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 169, Map = "#.#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 170, Map = "#.#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 171, Map = "#.#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 172, Map = "#.#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 173, Map = "#.#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 174, Map = "#.#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 175, Map = "#.#" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 176, Map = "#.#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 177, Map = "#.#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 178, Map = "#.#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 179, Map = "#.#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 180, Map = "#.#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 181, Map = "#.#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 182, Map = "#.#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 183, Map = "#.#" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 184, Map = "#.#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "...", Expected = true},
                new { Iteration = 185, Map = "#.#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "..#", Expected = true},
                new { Iteration = 186, Map = "#.#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 187, Map = "#.#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 188, Map = "#.#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#..", Expected = true},
                new { Iteration = 189, Map = "#.#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 190, Map = "#.#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 191, Map = "#.#" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 192, Map = "##." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 193, Map = "##." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 194, Map = "##." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 195, Map = "##." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".##", Expected = true},
                new { Iteration = 196, Map = "##." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 197, Map = "##." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 198, Map = "##." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "##.", Expected = true},
                new { Iteration = 199, Map = "##." + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "###", Expected = true},
                new { Iteration = 200, Map = "##." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 201, Map = "##." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 202, Map = "##." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 203, Map = "##." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".##", Expected = true},
                new { Iteration = 204, Map = "##." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 205, Map = "##." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 206, Map = "##." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 207, Map = "##." + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 208, Map = "##." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 209, Map = "##." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 210, Map = "##." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 211, Map = "##." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".##", Expected = true},
                new { Iteration = 212, Map = "##." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 213, Map = "##." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 214, Map = "##." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 215, Map = "##." + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 216, Map = "##." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "...", Expected = true},
                new { Iteration = 217, Map = "##." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "..#", Expected = true},
                new { Iteration = 218, Map = "##." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 219, Map = "##." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 220, Map = "##." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 221, Map = "##." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 222, Map = "##." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 223, Map = "##." + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 224, Map = "###" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 225, Map = "###" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 226, Map = "###" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".#.", Expected = true},
                new { Iteration = 227, Map = "###" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + ".##", Expected = true},
                new { Iteration = 228, Map = "###" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 229, Map = "###" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 230, Map = "###" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "##.", Expected = true},
                new { Iteration = 231, Map = "###" + Environment.NewLine
                                           + "..." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 232, Map = "###" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 233, Map = "###" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 234, Map = "###" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 235, Map = "###" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 236, Map = "###" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 237, Map = "###" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 238, Map = "###" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 239, Map = "###" + Environment.NewLine
                                           + "..#" + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 240, Map = "###" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 241, Map = "###" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 242, Map = "###" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 243, Map = "###" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 244, Map = "###" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 245, Map = "###" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 246, Map = "###" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 247, Map = "###" + Environment.NewLine
                                           + "#.." + Environment.NewLine
                                           + "###", Expected = false},
                new { Iteration = 248, Map = "###" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "...", Expected = false},
                new { Iteration = 249, Map = "###" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "..#", Expected = false},
                new { Iteration = 250, Map = "###" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".#.", Expected = false},
                new { Iteration = 251, Map = "###" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + ".##", Expected = false},
                new { Iteration = 252, Map = "###" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#..", Expected = false},
                new { Iteration = 253, Map = "###" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "#.#", Expected = false},
                new { Iteration = 254, Map = "###" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "##.", Expected = false},
                new { Iteration = 255, Map = "###" + Environment.NewLine
                                           + "#.#" + Environment.NewLine
                                           + "###", Expected = false},
            };
            foreach (var item in sampleData)
            {
                Dungeon testDungeon = DungeonFactory.GetDungeonBySimpleMap(item.Map);
                bool expected = item.Expected;

                bool actual = testDungeon.IsPossibleDoorway(new Location(1, 1));

                Assert.AreEqual(expected, actual, "Iteration<"+item.Iteration+">. Map:" + Environment.NewLine + item.Map);
            }
        }

        /// <summary>
        ///A test for CreatureList
        ///</summary>
        [TestMethod]
        public void IsInDungeonXYTest()
        {
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for CreatureList
        ///</summary>
        [TestMethod]
        public void IsInDungeonLocationTest()
        {
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #region Additional test attributes

        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //

        #endregion Additional test attributes
    }
}