﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CavesOfTheDragonAge.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CavesOfTheDragonAge.Common.Tests
{
    [TestClass()]
    public class StatPointTests
    {
        [TestMethod()]
        public void StatPointConstructorTest()
        {
            var sampleData = new[]
            {
                new { Row = 0, Actual = new StatPoint(), Current = 0, Minimum = 0, Maximum = 0 },
                new { Row = 1, Actual = new StatPoint(0), Current = 0, Minimum = 0, Maximum = 0 },
                new { Row = 2, Actual = new StatPoint(10), Current = 10, Minimum = 0, Maximum = 10 },
                new { Row = 3, Actual = new StatPoint(-1), Current = -1, Minimum = -1, Maximum = -1 },
                new { Row = 4, Actual = new StatPoint(-10), Current = -10, Minimum = -10, Maximum = -10 },
                new { Row = 5, Actual = new StatPoint(5, 10), Current = 5, Minimum = 0, Maximum = 10 },
                new { Row = 6, Actual = new StatPoint(-1, 10), Current = -1, Minimum = -1, Maximum = 10 },
                new { Row = 7, Actual = new StatPoint(11, 10), Current = 10, Minimum = 0, Maximum = 10 },
                new { Row = 8, Actual = new StatPoint(5, 1, 10), Current = 5, Minimum = 1, Maximum = 10 },
                new { Row = 9, Actual = new StatPoint(5, 4, 10), Current = 5, Minimum = 4, Maximum = 10 },
                new { Row = 10, Actual = new StatPoint(0, 1, 10), Current = 1, Minimum = 1, Maximum = 10 },
                new { Row = 11, Actual = new StatPoint(11, 1, 10), Current = 10, Minimum = 1, Maximum = 10 },
                new { Row = 12, Actual = new StatPoint(5, 1, -1), Current = 1, Minimum = 1, Maximum = -1 },
                new { Row = 13, Actual = new StatPoint(50, 5, 4), Current = 5, Minimum = 5, Maximum = 4 },
            };
            foreach (var item in sampleData)
            {
                StatPoint expected = new StatPoint();
                expected.Minimum = item.Minimum;
                expected.Maximum = item.Maximum;
                expected.Current = item.Current;

                StatPoint actual = item.Actual;

                Assert.AreEqual((expected.Current == actual.Current && expected.Minimum == actual.Minimum && expected.Maximum == actual.Maximum), true,
                    "Row " + item.Row + ". Expected:<" + expected + ">. Actual:<" + actual + ">.");
            }
        }

        [TestMethod()]
        public void StatPointToStringTest()
        {
            var sampleData = new[]
            {
                new { Row = 0, Expected = "0 in [0,0]", Actual = new StatPoint() },
                new { Row = 1, Expected = "0 in [0,0]", Actual = new StatPoint(0) },
                new { Row = 2, Expected = "5 in [0,10]", Actual = new StatPoint(5, 10) },
                new { Row = 3, Expected = "5 in [1,10]", Actual = new StatPoint(5, 1, 10) },
            };
            foreach (var item in sampleData)
            {
                string actual = item.Actual.ToString();

                Assert.AreEqual(item.Expected, actual, "Row " + item.Row + ".");
            }
        }

        [TestMethod()]
        public void StatPointImplicitOperatorStatPointToIntTest()
        {
            var sampleData = new[]
            {
                new { Row = 0, Expected = 1, Actual = new StatPoint(1) },
                new { Row = 1, Expected = 0, Actual = new StatPoint(0) },
                new { Row = 2, Expected = 5, Actual = new StatPoint(5, 10) },
                new { Row = 3, Expected = 5, Actual = new StatPoint(5, 1, 10) },
            };
            foreach (var item in sampleData)
            {
                int actual = item.Actual;

                Assert.AreEqual(item.Expected, actual, "Row " + item.Row + ".");
            }
        }

        [TestMethod()]
        public void StatPointImplicitOperatorIntToStatPointTest()
        {
            var sampleData = new[]
            {
                new { Row = 0, Expected = 1, Actual = new StatPoint(1) },
                new { Row = 1, Expected = 0, Actual = new StatPoint(0) },
                new { Row = 2, Expected = 5, Actual = new StatPoint(5, 10) },
                new { Row = 3, Expected = 5, Actual = new StatPoint(5, 1, 10) },
            };
            foreach (var item in sampleData)
            {
                int actual = item.Actual;

                Assert.AreEqual(item.Expected, actual, "Row " + item.Row + ".");
            }
        }

        [TestMethod()]
        public void StatPointOperatorStatPointPlusIntTest()
        {
            var sampleData = new[]
            {
                new { Row = 0, Expected = new StatPoint(10, 0, 10), Base = new StatPoint(5, 0, 10), Add = 5 },
                new { Row = 0, Expected = new StatPoint(9, 0, 9), Base = new StatPoint(5, 0, 9), Add = 5 },
            };
            foreach (var item in sampleData)
            {
                StatPoint actual = item.Base + item.Add;

                Assert.AreEqual(item.Expected.ToString(), actual.ToString(), "Row " + item.Row + ".");
            }
        }

        [TestMethod()]
        public void StatPointOperatorStatPointMinusIntTest()
        {
            var sampleData = new[]
            {
                new { Row = 0, Expected = new StatPoint(0, 0, 10), Base = new StatPoint(5, 0, 10), Subtract = 5 },
                new { Row = 0, Expected = new StatPoint(0, 0, 10), Base = new StatPoint(4, 0, 10), Subtract = 5 },
            };
            foreach (var item in sampleData)
            {
                StatPoint actual = item.Base - item.Subtract;

                Assert.AreEqual(item.Expected.ToString(), actual.ToString(), "Row " + item.Row + ".");
            }
        }

        [TestMethod()]
        public void StatPointAdditionOperatorTest()
        {
            var sampleData = new[]
            {
                new { Row = 0, Expected = new StatPoint(10, 0, 10), Base = new StatPoint(5, 0, 10), Add = 5 },
                new { Row = 0, Expected = new StatPoint(9, 0, 9), Base = new StatPoint(5, 0, 9), Add = 5 },
            };
            foreach (var item in sampleData)
            {
                StatPoint actual = item.Base;
                actual += item.Add;

                Assert.AreEqual(item.Expected.ToString(), actual.ToString(), "Row " + item.Row + ".");
            }
        }

        [TestMethod()]
        public void StatPointSubtractionOperatorTest()
        {
            var sampleData = new[]
            {
                new { Row = 0, Expected = new StatPoint(0, 0, 10), Base = new StatPoint(5, 0, 10), Subtract = 5 },
                new { Row = 0, Expected = new StatPoint(0, 0, 10), Base = new StatPoint(4, 0, 10), Subtract = 5 },
            };
            foreach (var item in sampleData)
            {
                StatPoint actual = item.Base;
                actual -= item.Subtract;

                Assert.AreEqual(item.Expected.ToString(), actual.ToString(), "Row " + item.Row + ".");
            }
        }
    }
}