﻿using System;
using System.Collections.Generic;
using CavesOfTheDragonAge.Common;

namespace CavesOfTheDragonAgeTests
{
    internal class UserInterfaceStub : IUserInterface
    {
        public void DisplayMessage(string message)
        {
            throw new NotImplementedException();
        }

        public void DisplayMessage(string message, System.Drawing.Color color)
        {
            throw new NotImplementedException();
        }

        public CreatureAction GetPlayerAction()
        {
            throw new NotImplementedException();
        }

        public void InitUi(int width = 80, int height = 50, string title = "Caves of the Dragon Age")
        {
            throw new NotImplementedException();
        }

        public void RenderAll(bool keepMessages = false)
        {
            throw new NotImplementedException();
        }

        public int DisplayMenu(string header, List<string> options, bool forceSelection = false, int width = 0)
        {
            throw new NotImplementedException();
        }

        public string GetTextFromUser(string prompt, string currentText = "", int maxCharacters = 50, int width = 0)
        {
            throw new NotImplementedException();
        }

        public int DisplayMenu(string header, List<string> options, bool forceSelection = false, int width = 0, string backgroundImageFilename = null)
        {
            throw new NotImplementedException();
        }

        public string PlayerId
        {
            get
            {
                return "PLAYER";
            }
            set
            {
            }
        }

        public bool UserCommandWaiting
        {
            get { throw new NotImplementedException(); }
        }
    }
}